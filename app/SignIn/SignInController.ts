import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { KrossrRequest } from '../KrossrRequest/KrossrRequest';
import { inject } from 'inversify';
import { AuthenticationService } from '../Authentication/AuthenticationService';
import { provide } from 'inversify-binding-decorators';
import { Body, Post, Request, Res, Route, TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { UserViewModel } from '../Users/UserViewModel';
import { SignInBodyViewModel } from './SignInBodyViewModel';

@provide(SignInController)
@Route('auth')
export class SignInController {
    constructor(
        @inject(AuthenticationService) private authenticationService: AuthenticationService,
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(UserViewModelMapper) private userMapper: UserViewModelMapper
    ) {
    }

    @Post('signin')
    public async signIn(
        @Request() req: KrossrRequest,
        @Body() body: SignInBodyViewModel,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ): Promise<UserViewModel> {
        try {
            let user = await this.authenticationService.authenticate(body);

            return new Promise((resolve, reject) => {
                req.login(user, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(this.userMapper.toViewModel(user));
                    }
                });
            });
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }
}
