export interface SignInBodyViewModel {
    username: string;
    password: string;
}
