import { SignInController } from './SignInController';
import { Container } from 'inversify';
import { ErrorHandler } from '../Error/ErrorHandler';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { MockRequest } from '../../spec/support/MockRequest';
import { MockResponse } from '../../spec/support/MockResponse';
import { AuthenticationService } from '../Authentication/AuthenticationService';
import { UserService } from '../Users/UserService';
import { User } from '../models/UserModel';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { TestHelpers } from '../../spec/support/TestHelpers';
import { UserRepository } from '../Users/UserRepository';
import { ErrorContainer } from '../../spec/containers/error-container';
import { SignInBodyViewModel } from './SignInBodyViewModel';

describe('SignInController', () => {
    let container: Container;
    let authenticationService: AuthenticationService;
    let controller: SignInController;
    let errorHandler: ErrorHandler;

    class MockSignInRequest extends MockRequest {
        body: SignInBodyViewModel;
    }

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(AuthenticationService).toSelf().inSingletonScope();
        container.bind(UserViewModelMapper).toSelf();
        container.bind(SignInController).toSelf();
        container.bind(UserService).toSelf();
        container.bind(UserRepository).toSelf().inSingletonScope();
        controller = container.get(SignInController);
        authenticationService = container.get(AuthenticationService);
        errorHandler = container.get(ErrorHandler);
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    it('should sign in', async () => {
        let request = new MockSignInRequest();
        request.login = (user: User, callback: (err: any) => void) => {
            callback(null);
        };

        let response = new MockResponse();
        let userService = container.get(UserService);


        spyOn(authenticationService, 'authenticate').and.returnValue(Promise.resolve(
            userService.createUserModel(TestHelpers.getTestUserAttributes())
        ));

        expect(await controller.signIn(request as any, request.body, response as any)).toBeTruthy();
    });
});
