export interface MailParams {
    to: string;
    from: string;
    subject: string;
    html: string;
}
