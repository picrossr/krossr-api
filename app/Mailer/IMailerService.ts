import { MailParams } from './MailParams';

export interface IMailerService {
    send(params: MailParams): void;
}
