import { MailerService } from './MailerService';

export class MailerSymbols {
    static MailerService = Symbol.for(nameof(MailerService));
}
