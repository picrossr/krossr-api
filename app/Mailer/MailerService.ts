/* istanbul ignore file */

/*
* this file is ignored in test coverage to avoid sending mail thru an api while running tests,
* and should be stubbed when it is used elsewhere
*/

import sgMail from '@sendgrid/mail';
import { MailParams } from './MailParams';
import { IMailerService } from './IMailerService';
import { provide } from 'inversify-binding-decorators';
import { MailerSymbols } from './MailerSymbols';

@provide(MailerSymbols.MailerService)
export class MailerService implements IMailerService {
    send(params: MailParams) {
        sgMail.setApiKey(process.env.SENDGRID_API_KEY);
        return sgMail.send(params);
    }
}
