import { ChangePasswordController } from './ChangePasswordController';
import { Container } from 'inversify';
import { PasswordService } from '../Password/PasswordService';
import { UserService } from '../Users/UserService';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { User } from '../models/UserModel';
import { UserRepository } from '../Users/UserRepository';
import { ErrorContainer } from '../../spec/containers/error-container';
import { TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ChangePasswordBodyViewModel } from './ChangePasswordBodyViewModel';
import { UserViewModel } from '../Users/UserViewModel';

describe('ChangePasswordController', () => {
    let container: Container;
    let controller: ChangePasswordController;
    let passwordService: PasswordService;
    let userService: UserService;
    let clientError: TsoaResponse<400, ErrorResponse>;
    let serverError: TsoaResponse<500, ErrorResponse>;

    class MockChangePasswordRequest {
        body: ChangePasswordBodyViewModel;
        user?: UserViewModel;
        login?: (user: User, callback: (err: any) => void) => void;
    }

    beforeEach(async () => {
        container = new Container();
        await new ErrorContainer().initialize(container);

        container.bind(PasswordService).toSelf().inSingletonScope();
        container.bind(ChangePasswordController).toSelf();
        container.bind(UserService).toSelf().inSingletonScope();
        container.bind(UserRepository).toSelf().inSingletonScope();
        container.bind(UserViewModelMapper).toSelf();
        controller = container.get(ChangePasswordController);
        userService = container.get(UserService);
        passwordService = container.get(PasswordService);
        clientError = jasmine.createSpy();
        serverError = jasmine.createSpy();
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    describe('#changePassword', () => {
        it('should detect if new password is missing', () => {
            let request: MockChangePasswordRequest = { body: { currentPassword: '', newPassword: '', verifyPassword: '' } };

            controller.changePassword(request as any, request.body, clientError, serverError);
            expect(clientError).toHaveBeenCalled();
        });

        it('should detect if passwords do not match', () => {
            let request: MockChangePasswordRequest = { body: { currentPassword: '', newPassword: 'newnewnewnew', verifyPassword: 'newnewnewneu' } };

            controller.changePassword(request as any, request.body, clientError, serverError);
            expect(clientError).toHaveBeenCalled();
        });

        it('should detect if no user is signed in', () => {
            let request: MockChangePasswordRequest = { body: { currentPassword: '', newPassword: 'newnewnewnew', verifyPassword: 'newnewnewnew' } };

            controller.changePassword(request as any, request.body, clientError, serverError);
            expect(clientError).toHaveBeenCalled();
        });

        it('should detect if the supposedly signed in user does not exist', async () => {
            let request: MockChangePasswordRequest = {
                body: { currentPassword: '', newPassword: 'newnewnewnew', verifyPassword: 'newnewnewnew' },
                user: { id: 1, username: 'rosalyn' }
            };

            spyOn(userService, 'getUserModel').and.returnValue(Promise.resolve(null));

            await controller.changePassword(request as any, request.body, clientError, serverError);
            expect(clientError).toHaveBeenCalled();
        });

        it('should detect if the current password is incorrect', async () => {
            let request: MockChangePasswordRequest = {
                body: { currentPassword: '', newPassword: 'newnewnewnew', verifyPassword: 'newnewnewnew' },
                user: { id: 1, username: 'rosalyn' }
            };

            spyOn(userService, 'getUserModel').and.returnValue(Promise.resolve({} as any));
            spyOn(passwordService, 'authenticate').and.returnValue(false);

            await controller.changePassword(request as any, request.body, clientError, serverError);
            expect(clientError).toHaveBeenCalled();
        });

        it('should set the new password if all is well', async () => {
            let request: MockChangePasswordRequest = {
                body: { currentPassword: 'current', newPassword: 'newnewnewnew', verifyPassword: 'newnewnewnew' },
                user: { id: 1, username: 'rosalyn' },
                login: (user, callback) => { callback(null); }
            };

            spyOn(userService, 'getUserModel').and.returnValue(Promise.resolve({} as any));
            spyOn(userService, 'saveUser');
            spyOn(passwordService, 'authenticate').and.returnValue(true);

            await controller.changePassword(request as any, request.body, clientError, serverError);
            expect(userService.saveUser).toHaveBeenCalled();
        });

        it('should handle a successful change', async () => {
            let request: MockChangePasswordRequest = {
                body: { currentPassword: 'current', newPassword: 'newnewnewnew', verifyPassword: 'newnewnewnew' },
                user: { id: 1, username: 'rosalyn' },
                login: (user, callback) => { callback(null); }
            };

            spyOn(userService, 'getUserModel').and.returnValue(Promise.resolve({} as any));
            spyOn(userService, 'saveUser');
            spyOn(passwordService, 'authenticate').and.returnValue(true);

            await controller.changePassword(request as any, request.body, clientError, serverError);
            expect(userService.saveUser).toHaveBeenCalled();
        });

        it('should handle a login error', async () => {
            let request: MockChangePasswordRequest = {
                body: { currentPassword: 'current', newPassword: 'newnewnewnew', verifyPassword: 'newnewnewnew' },
                user: { id: 1, username: 'rosalyn' },
                login: (user, callback) => { callback('boop'); }
            };

            spyOn(userService, 'getUserModel').and.returnValue(Promise.resolve({} as any));
            spyOn(userService, 'saveUser');
            spyOn(passwordService, 'authenticate').and.returnValue(true);

            let fn = async () => await controller.changePassword(request as any, request.body, clientError, serverError);
            await expectAsync(fn()).toBeRejected();
        });

        it('should handle an unknown error', async () => {
            let request: MockChangePasswordRequest = {
                body: { currentPassword: 'current', newPassword: 'newnewnewnew', verifyPassword: 'newnewnewnew' },
                user: { id: 1, username: 'rosalyn' },
                login: (user, callback) => { callback(null); }
            };

            spyOn(userService, 'getUserModel').and.throwError('womp womp');

            await controller.changePassword(request as any, request.body, clientError, serverError);
            expect(serverError).toHaveBeenCalled();
        });
    });
});
