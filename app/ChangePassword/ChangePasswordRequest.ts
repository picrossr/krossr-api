import { KrossrRequest } from '../KrossrRequest/KrossrRequest';
import { ChangePasswordBodyViewModel } from './ChangePasswordBodyViewModel';

export interface ChangePasswordRequest extends KrossrRequest<any, ChangePasswordBodyViewModel> {
}
