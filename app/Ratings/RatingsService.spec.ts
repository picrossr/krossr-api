import { RatingsService } from './RatingsService';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { RatingsRepository } from './RatingsRepository';

describe('RatingsService', () => {
    let service: RatingsService;
    let repo: RatingsRepository;

    beforeEach(async () => {
        let DIContainer = await new SequelizeContainer().initialize();

        DIContainer.bind(RatingsService).toSelf();
        DIContainer.bind(RatingsRepository).toSelf().inSingletonScope();

        service = DIContainer.get(RatingsService);
        repo = DIContainer.get(RatingsRepository);
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });

    it('should upsert', () => {
        spyOn(repo, 'upsert');
        service.upsertRating({ id: 1 } as any, { id: 1 } as any, { rating: 1 } as any);
        expect(repo.upsert).toHaveBeenCalled();
    });
});
