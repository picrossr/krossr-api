import { Level } from '../models/LevelModel';
import { User } from '../models/UserModel';
import { RatingCreationAttributes } from '../models/RatingModel';
import { inject } from 'inversify';
import { RatingsRepository } from './RatingsRepository';
import { provide } from 'inversify-binding-decorators';
import { UpsertRatingBodyViewModel } from './UpsertRatingBodyViewModel';

@provide(RatingsService)
export class RatingsService {
    constructor(
        @inject(RatingsRepository) private ratingsRepository: RatingsRepository
    ) {
    }

    /**
     * Add or replace a Rating. Each user should only be able to have one rating for each level.
     */
    async upsertRating(level: Level, user: User, rating: UpsertRatingBodyViewModel): Promise<void> {
        let attributes: RatingCreationAttributes = {
            levelId: level.id,
            userId: user.id,
            rating: rating.rating
        };

        return this.ratingsRepository.upsert(attributes);
    }
}
