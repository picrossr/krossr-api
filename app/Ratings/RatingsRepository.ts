/* istanbul ignore file */

import { RatingCreationAttributes, Rating } from '../models/RatingModel';
import { provide } from 'inversify-binding-decorators';

@provide(RatingsRepository)
export class RatingsRepository {
    async upsert(attributes: RatingCreationAttributes) {
        let where = {
            userId: attributes.userId,
            levelId: attributes.levelId
        };

        let value = await Rating.findOrCreate({
            where ,
            defaults: attributes
        });

        let created = value[1];

        if (created) {
            return Promise.resolve();
        }

        await Rating.update({
            rating: attributes.rating
        }, { where });

        return Promise.resolve();
    }
}
