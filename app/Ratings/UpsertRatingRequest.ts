import { KrossrRequest } from '../KrossrRequest/KrossrRequest';
import { Level } from '../models/LevelModel';
import { UpsertRatingBodyViewModel } from './UpsertRatingBodyViewModel';

export interface UpsertRatingRequest extends KrossrRequest<any, UpsertRatingBodyViewModel> {
    level: Level;
}
