import { RatingsController } from './RatingsController';
import { Container } from 'inversify';
import { ErrorHandler } from '../Error/ErrorHandler';
import { User } from '../models/UserModel';
import { RatingsService } from './RatingsService';
import { RatingsRepository } from './RatingsRepository';
import { ErrorContainer } from '../../spec/containers/error-container';
import { UpsertRatingBodyViewModel } from './UpsertRatingBodyViewModel';
import { UserViewModel } from '../Users/UserViewModel';

describe('RatingsController', () => {
    let container: Container;
    let controller: RatingsController;
    let errorHandler: ErrorHandler;
    let ratingsService: RatingsService;

    class MockRatingsRequest {
        body: UpsertRatingBodyViewModel;
        user?: UserViewModel;
        login?: (user: User, callback: (err: any) => void) => void;
        params: { levelId: string };

        constructor(levelId: number) {
            this.params = {
                levelId: levelId.toString()
            };
        }
    }

    beforeEach(async () => {
        container = new Container();
        await new ErrorContainer().initialize(container);
        container.bind(RatingsService).toSelf().inSingletonScope();
        container.bind(RatingsController).toSelf();
        container.bind(RatingsRepository).toSelf().inSingletonScope();
        controller = container.get(RatingsController);
        errorHandler = container.get(ErrorHandler);
        ratingsService = container.get(RatingsService);
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    describe('#upsert rating', () => {
        it('should catch a failure', () => {
            spyOn(ratingsService, 'upsertRating').and.throwError('something');

            let request = new MockRatingsRequest(1);
            let errorResponse = jasmine.createSpy();

            controller.upsertRating(request.params.levelId, request.body, request as any, errorResponse);
            expect(errorResponse).toHaveBeenCalled();
        });

        it('should handle a success', async () => {
            spyOn(ratingsService, 'upsertRating').and.returnValue(Promise.resolve());

            let request = new MockRatingsRequest(1);
            let errorResponse = jasmine.createSpy();

            await controller.upsertRating(request.params.levelId, request.body, request as any, errorResponse);
            expect(ratingsService.upsertRating).toHaveBeenCalled();
        });
    });
});
