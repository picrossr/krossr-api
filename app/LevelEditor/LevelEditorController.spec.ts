import { LevelEditorController } from './LevelEditorController';
import { Container } from 'inversify';
import { LevelEditorService } from './LevelEditorService';
import { SizeFormatter } from '../Size/SizeFormatter';

describe('LevelEditorController', () => {
    let container: Container;
    let controller: LevelEditorController;
    let service: LevelEditorService;

    beforeEach(() => {
        container = new Container();
        container.bind(LevelEditorService).toSelf().inSingletonScope();
        container.bind(SizeFormatter).toSelf();
        container.bind(LevelEditorController).toSelf();
        controller = container.get(LevelEditorController);
        service = container.get(LevelEditorService);
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    it('should get options', () => {
        spyOn(service, 'getOptions');
        controller.getLevelEditorOptions();
        expect(service.getOptions).toHaveBeenCalled();
    });
});
