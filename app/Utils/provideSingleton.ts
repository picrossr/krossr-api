import { fluentProvide } from 'inversify-binding-decorators';
import { interfaces } from 'inversify';

// tslint:disable-next-line:only-arrow-functions
export const provideSingleton = function<T>(identifier: interfaces.ServiceIdentifier<T>, force: boolean = false) {
    return fluentProvide(identifier).inSingletonScope().done(force);
};
