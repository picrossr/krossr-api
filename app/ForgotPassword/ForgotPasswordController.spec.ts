import { Container } from 'inversify';
import { UserService } from '../Users/UserService';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { ForgotPasswordController } from './ForgotPasswordController';
import { MockMailerService } from '../../spec/support/MockMailerService';
import { IMailerService } from '../Mailer/IMailerService';
import { MailerSymbols } from '../Mailer/MailerSymbols';
import { MailerService } from '../Mailer/MailerService';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { TestHelpers } from '../../spec/support/TestHelpers';
import { UserRepository } from '../Users/UserRepository';
import { CryptoService } from '../Crypto/CryptoService';
import { ErrorContainer } from '../../spec/containers/error-container';
import { HtmlRenderer } from '../HtmlRenderer/HtmlRenderer';
import { ForgotPasswordBodyViewModel } from './ForgotPasswordBodyViewModel';

describe('ForgotPasswordController', () => {
    let container: Container;
    let controller: ForgotPasswordController;
    let cryptoService: CryptoService;
    let htmlRenderer: HtmlRenderer;
    let userService: UserService;
    let mailerService: MailerService;
    let clientError: jasmine.Spy;
    let serverError: jasmine.Spy;

    class MockForgotPasswordRequest {
        body: ForgotPasswordBodyViewModel;
        headers?: { host: string };
    }

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(CryptoService).toSelf().inSingletonScope();
        container.bind(HtmlRenderer).toSelf().inSingletonScope();
        container
            .bind<IMailerService>(MailerSymbols.MailerService)
            .to(MockMailerService)
            .inSingletonScope();
        container.bind(ForgotPasswordController).toSelf();
        container.bind(UserService).toSelf().inSingletonScope();
        container.bind(UserRepository).toSelf().inSingletonScope();
        container.bind(UserViewModelMapper).toSelf();
        controller = container.get(ForgotPasswordController);
        htmlRenderer = container.get(HtmlRenderer);
        userService = container.get(UserService);
        mailerService = container.get(MailerSymbols.MailerService);
        cryptoService = container.get(CryptoService);
        clientError = jasmine.createSpy();
        serverError = jasmine.createSpy();
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    describe('#forgot', () => {
        it('should detect a missing username', async () => {
            let request: MockForgotPasswordRequest = { body: { username: '' } };

            await controller.forgot(request as any, request.body, clientError, serverError);
            expect(clientError).toHaveBeenCalled();
        });

        it('should not detect if the username does not exist', async () => {
            let request: MockForgotPasswordRequest = {
                body: { username: 'rosalyn' },
            };

            spyOn(userService, 'getUserModelByUsername').and.returnValue(Promise.resolve(null));

            await controller.forgot(request as any, request.body, clientError, serverError);
            expect(clientError).not.toHaveBeenCalled();
        });

        it('should fire the email if the username does exist', async () => {
            spyOn(mailerService, 'send');

            let request: MockForgotPasswordRequest = {
                body: {
                    username: 'rosalyn',
                },
                headers: {
                    host: 'something',
                },
            };

            let expectedUser = userService.createUserModel(TestHelpers.getTestUserAttributes());

            spyOn(userService, 'getUserModelByUsername').and.returnValue(
                Promise.resolve(expectedUser)
            );
            spyOn(userService, 'saveUser').and.returnValue(Promise.resolve(expectedUser));

            await controller.forgot(request as any, request.body, clientError, serverError);
            expect(userService.saveUser).toHaveBeenCalled();
            expect(mailerService.send).toHaveBeenCalled();
        });

        it('should handle an email rendering error', async () => {
            spyOn(mailerService, 'send');

            let request: MockForgotPasswordRequest = {
                body: {
                    username: 'rosalyn',
                },
                headers: {
                    host: 'something',
                },
            };

            spyOn(htmlRenderer, 'render').and.throwError('wat');

            let expectedUser = userService.createUserModel(TestHelpers.getTestUserAttributes());

            spyOn(userService, 'getUserModelByUsername').and.returnValue(
                Promise.resolve(expectedUser)
            );
            spyOn(userService, 'saveUser').and.returnValue(Promise.resolve(expectedUser));

            await controller.forgot(request as any, request.body, clientError, serverError);
            expect(mailerService.send).not.toHaveBeenCalled();
        });

        it('should handle a crypto error', async () => {
            let request: MockForgotPasswordRequest = {
                body: { username: 'rosalyn' },
            };

            spyOn(userService, 'getUserModelByUsername').and.returnValue(
                Promise.resolve(userService.createUserModel(TestHelpers.getTestUserAttributes()))
            );

            spyOn(cryptoService, 'randomBytes').and.returnValue(Promise.reject());

            await controller.forgot(request as any, request.body, clientError, serverError);
            expect(serverError).toHaveBeenCalled();
        });
    });
});
