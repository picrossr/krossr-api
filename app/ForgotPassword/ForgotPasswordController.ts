import { EnvironmentConfiguration } from '../../config/environmentConfig';
import { User } from '../models/UserModel';
import { MailerService } from '../Mailer/MailerService';
import { inject } from 'inversify';
import { IEnvironmentConfiguration } from '../../config/env/IEnvironmentConfiguration';
import { ForgotPasswordRequest } from './ForgotPasswordRequest';
import { MailerSymbols } from '../Mailer/MailerSymbols';
import { UserService } from '../Users/UserService';
import { CryptoService } from '../Crypto/CryptoService';
import { provide } from 'inversify-binding-decorators';
import { HtmlRenderer } from '../HtmlRenderer/HtmlRenderer';
import { Body, Post, Request, Res, Route, TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { ForgotPasswordBodyViewModel } from './ForgotPasswordBodyViewModel';

@provide(ForgotPasswordController)
@Route('auth')
export class ForgotPasswordController {
    private config: IEnvironmentConfiguration;

    constructor(
        @inject(CryptoService) private cryptoService: CryptoService,
        @inject(EnvironmentConfiguration)
        private environmentConfiguration: EnvironmentConfiguration,
        @inject(HtmlRenderer) private htmlRenderer: HtmlRenderer,
        @inject(ErrorMessageService)
        private errorMessageService: ErrorMessageService,
        @inject(MailerSymbols.MailerService) private mailerService: MailerService,
        @inject(UserService) private userService: UserService
    ) {
        this.config = this.environmentConfiguration.getConfiguration();
    }

    /**
     * Sends a 'forgot password' email'
     * @returns nothing
     */
    @Post('forgot')
    public async forgot(
        @Request() req: ForgotPasswordRequest,
        @Body() body: ForgotPasswordBodyViewModel,
        @Res() clientError: TsoaResponse<400, ErrorResponse>,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ): Promise<void> {
        if (!body.username) {
            return clientError(400, { message: 'Username required' });
        }

        try {
            let token = await this.generateRandomToken();
            let user = await this.userService.getUserModelByUsername(body.username);

            if (user) {
                this.updateUser(user, token);

                let emailHTML = await this.renderEmail(req, user, token);

                await this.sendEmail(emailHTML, user);
            }
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }

    /**
     * Do not use a custom implementation here
     * @returns a random token
     */
    private async generateRandomToken(): Promise<string> {
        return await this.cryptoService.randomBytes(20);
    }

    /**
     * Calculate a time based on the current time for the token to expire
     * @returns the expiration Date
     */
    private getExpirationDateTime(): Date {
        let now = new Date();
        now.setHours(now.getHours() + 1); // add 1 hour

        return now;
    }

    /**
     * Provides the User a window with which to reset the password
     * @returns the same User
     */
    private async updateUser(user: User, token: string): Promise<User> {
        user.resetPasswordToken = token;
        user.resetPasswordExpires = this.getExpirationDateTime();

        return await this.userService.saveUser(user);
    }

    /**
     * @returns the email Email for the given user
     */
    private async renderEmail(req: ForgotPasswordRequest, user: User, token: string) {
        return new Promise<string>((resolve, reject) => {
            try {
                let emailHTML = this.htmlRenderer.render(
                    'app/views/templates/reset-password-email.server.view.html',
                    {
                        name: user.username,
                        appName: this.config.app.title,
                        url: 'http://' + req.headers.host + '/password/reset/' + token,
                    }
                );

                resolve(emailHTML);
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Ships the email off
     */
    private async sendEmail(emailHTML: string, user: User): Promise<void> {
        let mailOptions = {
            to: user.email,
            from: this.config.mailer.from,
            subject: 'Password Reset',
            html: emailHTML,
        };

        await this.mailerService.send(mailOptions);
    }
}
