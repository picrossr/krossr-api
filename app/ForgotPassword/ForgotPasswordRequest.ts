import { KrossrRequest } from '../KrossrRequest/KrossrRequest';
import { ForgotPasswordBodyViewModel } from './ForgotPasswordBodyViewModel';

export interface ForgotPasswordRequest extends KrossrRequest<any, ForgotPasswordBodyViewModel> {
}
