export const enum Securities {
    RequiresLogin = 'requires_login',
    CanCreate = 'can_create',
    CanUpdate = 'can_update',
    CanDelete = 'can_delete',
}
