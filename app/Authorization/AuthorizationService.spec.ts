import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { Level } from '../models/LevelModel';
import { User } from '../models/UserModel';
import { AuthorizationService } from './AuthorizationService';
import { Resources } from './Resources';
import { Roles } from './Roles';

describe('AuthorizationService', () => {
    let service: AuthorizationService;

    function getLevel() {
        let level = Level.build({
            name: 'blahblah',
            size: 25,
            layout: '',
            userId: 42
        });

        return level;
    }

    function getAdmin() {
        let admin = User.build({
            email: '',
            username: 'admin',
            role: Roles.Admin,
            hashedPassword: '',
            salt: '',
            provider: 'local'
        });

        admin.id = 1;

        return admin;
    }

    function getUser(id: number) {
        let user = User.build({
            email: '',
            username: 'user',
            role: Roles.User,
            hashedPassword: '',
            salt: '',
            provider: 'local'
        });

        user.id = id;

        return user;
    }

    beforeEach(async () => {
        let container = await new SequelizeContainer().initialize();
        service = container.get(AuthorizationService);
        service.configure();
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });

    it('should not allow logged out users to edit levels', () => {
        let result = service.canUpdate(getLevel(), null);

        expect(result).toBeFalsy();
    });

    it('should not allow logged out users to delete levels', () => {
        let result = service.canDelete(getLevel(), null);

        expect(result).toBeFalsy();
    });

    it(`should allow admins to edit levels they didn't create`, () => {
        let result = service.canUpdate(getLevel(), getAdmin());

        expect(result).toBeTruthy();
    });

    it(`should not allow users to edit levels they didn't create`, () => {
        let result = service.canUpdate(getLevel(), getUser(1));

        expect(result).toBeFalsy();
    });

    it('should allow users to edit levels they did create', () => {
        let result = service.canUpdate(getLevel(), getUser(42));

        expect(result).toBeTruthy();
    });

    it(`should allow admins to delete levels they didn't create`, () => {
        let result = service.canDelete(getLevel(), getAdmin());

        expect(result).toBeTruthy();
    });

    it(`should not allow users to delete levels they didn't create`, () => {
        let result = service.canDelete(getLevel(), getUser(1));

        expect(result).toBeFalsy();
    });

    it('should allow users to delete levels they did create', () => {
        let result = service.canDelete(getLevel(), getUser(42));

        expect(result).toBeTruthy();
    });

    it('should allow admins to ban layouts', () => {
        let result = service.canCreate(Resources.BannedLayout, getAdmin());

        expect(result).toBeTruthy();
    });

    it('should not allow users to ban layouts', () => {
        let result = service.canCreate(Resources.BannedLayout, getUser(42));

        expect(result).toBeFalsy();
    });
});
