import { AccessControl } from 'accesscontrol';
import { User } from '../models/UserModel';
import { UserOwnedResource } from '../models/UserOwnedResource';
import { provideSingleton } from '../Utils/provideSingleton';
import { Resources } from './Resources';
import { Roles } from './Roles';

@provideSingleton(AuthorizationService)
export class AuthorizationService {
    static accessControl: AccessControl = new AccessControl();

    configure() {
        AuthorizationService.accessControl.grant(Roles.Admin)
            .deleteAny(Resources.Level)
            .updateAny(Resources.Level)
            .createAny(Resources.BannedLayout);

        AuthorizationService.accessControl.grant(Roles.User)
            .deleteOwn(Resources.Level)
            .updateOwn(Resources.Level);
    }

    canCreate(resource: Resources, user: User) {
        return this.canCreateAny(resource, user) || this.canCreateOwn(resource, user);
    }

    canDelete(model: UserOwnedResource, user: User) {
        return this.canDeleteAny(model, user) || this.canDeleteOwn(model, user);
    }

    canUpdate(model: UserOwnedResource, user: User) {
        return this.canUpdateAny(model, user) || this.canUpdateOwn(model, user);
    }

    private getQuery(user: User) {
        return AuthorizationService.accessControl.can(user.role);
    }

    private canCreateAny(resource: Resources, user: User) {
        return user && this.getQuery(user).createAny(resource).granted;
    }

    private canCreateOwn(resource: Resources, user: User) {
        return user && this.getQuery(user).createOwn(resource).granted;
    }

    private canDeleteAny(model: UserOwnedResource, user: User) {
        return model && user && this.getQuery(user).deleteAny(model.resource).granted;
    }

    private canDeleteOwn(model: UserOwnedResource, user: User) {
        return model && user && this.getQuery(user).deleteOwn(model.resource).granted && model.userId === user.id;
    }

    private canUpdateAny(model: UserOwnedResource, user: User) {
        return model && user && this.getQuery(user).updateAny(model.resource).granted;
    }

    private canUpdateOwn(model: UserOwnedResource, user: User) {
        return model && user && this.getQuery(user).updateOwn(model.resource).granted && model.userId === user.id;
    }
}
