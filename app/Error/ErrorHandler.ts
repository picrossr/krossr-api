import { KrossrErrorResponse } from '../KrossrResponse/KrossrErrorResponse';
import { provide } from 'inversify-binding-decorators';
import { inject } from 'inversify';
import { ErrorMessageService } from './ErrorMessageService';

@provide(ErrorHandler)
export class ErrorHandler {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService
    ) {
    }

    public sendUnauthenticatedErrorResponse(res: KrossrErrorResponse) {
        return this.sendErrorResponse(res, 'User is not logged in', 401);
    }

    public sendForbiddenErrorResponse(res: KrossrErrorResponse) {
        return this.sendErrorResponse(res, 'User is not allowed', 403);
    }

    public sendClientErrorResponse(res: KrossrErrorResponse, err: string) {
        return this.sendErrorResponse(res, err, 400);
    }

    public sendServerErrorResponse(res: KrossrErrorResponse, err: string) {
        return this.sendErrorResponse(res, err, 500);
    }

    public sendErrorResponse(res: KrossrErrorResponse, err: string, statusCode: number) {
        return res.status(statusCode).send({
            message: err
        });
    }

    public sendUnknownClientErrorResponse(res: KrossrErrorResponse, err: any) {
        return this.sendClientErrorResponse(res, this.errorMessageService.getErrorMessage(err));
    }

    public sendUnknownServerErrorResponse(res: KrossrErrorResponse, err: any) {
        return this.sendServerErrorResponse(res, this.errorMessageService.getErrorMessage(err));
    }
}


