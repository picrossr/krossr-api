import { Container } from 'inversify';
import { ErrorContainer } from '../../spec/containers/error-container';
import { AuthorizationService } from '../Authorization/AuthorizationService';
import { LevelRepository } from '../Levels/LevelRepository';
import { LevelService } from '../Levels/LevelService';
import { LevelViewModelMapper } from '../Levels/LevelViewModelMapper';
import { BannedLayoutController } from './BannedLayoutController';
import { BannedLayoutRepository } from './BannedLayoutRepository';
import { BannedLayoutService } from './BannedLayoutService';

describe('BannedLayoutController', () => {
    let container: Container;
    let controller: BannedLayoutController;

    beforeEach(async () => {
        container = new Container();
        await new ErrorContainer().initialize(container);
        container.bind(AuthorizationService).toSelf();
        container.bind(BannedLayoutRepository).toSelf();
        container.bind(BannedLayoutService).toSelf();
        container.bind(BannedLayoutController).toSelf();
        container.bind(LevelService).toSelf();
        container.bind(LevelViewModelMapper).toSelf();
        container.bind(LevelRepository).toSelf();
        controller = container.get(BannedLayoutController);
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });
});
