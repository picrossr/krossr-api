import { inject } from 'inversify';
import { provide } from 'inversify-binding-decorators';
import { BannedLayoutRepository } from './BannedLayoutRepository';

@provide(BannedLayoutService)
export class BannedLayoutService {
    static defaultBanMessage = 'This layout has been banned';

    constructor(
        @inject(BannedLayoutRepository) private bannedLayoutRepository: BannedLayoutRepository
    ) {
    }

    async banLayout(layout: string, message: string = BannedLayoutService.defaultBanMessage) {
        await this.bannedLayoutRepository.upsert({ layout, message });
    }

    async isBanned(layout: string) {
        let result = await this.bannedLayoutRepository.getBannedLayoutByLayout(layout);

        return !!result;
    }

    async getMessageForLayout(layout: string) {
        let result = await this.bannedLayoutRepository.getBannedLayoutByLayout(layout);

        return result && result.message ? result.message : BannedLayoutService.defaultBanMessage;
    }
}
