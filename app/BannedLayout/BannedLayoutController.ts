import { inject } from 'inversify';
import { provide } from 'inversify-binding-decorators';
import { Body, Path, Post, Request, Res, Route, Security, TsoaResponse } from 'tsoa';
import { Resources } from '../Authorization/Resources';
import { Securities } from '../Authorization/Securities';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { ErrorResponse } from '../Error/ErrorResponse';
import { LevelRequest } from '../Levels/LevelRequest';
import { LevelService } from '../Levels/LevelService';
import { BannedLayoutService } from './BannedLayoutService';
import { CreateBannedLayoutBodyViewModel } from './CreateBannedLayoutBodyViewModel';

@provide(BannedLayoutController)
@Route('levels')
export class BannedLayoutController {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(BannedLayoutService) private bannedLayoutService: BannedLayoutService,
        @inject(LevelService) private levelService: LevelService
    ) {
    }

    @Post('{levelId}/ban')
    @Security(Securities.RequiresLogin)
    @Security(Securities.CanCreate, [Resources.BannedLayout])
    public async banLevel(
        @Path() levelId: string,
        @Body() body: CreateBannedLayoutBodyViewModel,
        @Request() req: LevelRequest<CreateBannedLayoutBodyViewModel>,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ) {
        try {
            await this.bannedLayoutService.banLayout(req.level.layout, body.message);
            await this.levelService.deleteLevel(req.level);
            return;
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }
}
