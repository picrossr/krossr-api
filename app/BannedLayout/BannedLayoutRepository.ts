/* istanbul ignore file */

import { provide } from 'inversify-binding-decorators';
import { BannedLayout, BannedLayoutCreationAttributes } from '../models/BannedLayoutModel';

@provide(BannedLayoutRepository)
export class BannedLayoutRepository {
    async upsert(attributes: BannedLayoutCreationAttributes) {
        let where = {
            layout: attributes.layout
        };

        await BannedLayout.findOrCreate({
            where,
            defaults: attributes
        });

        return Promise.resolve();
    }

    async getBannedLayoutByLayout(layout: string) {
        return await BannedLayout.findOne({ where: { layout }});
    }
}
