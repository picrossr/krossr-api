import { Response } from 'express';
import { inject } from 'inversify';
import { provide } from 'inversify-binding-decorators';
import { AuthorizationService } from '../Authorization/AuthorizationService';
import { Resources } from '../Authorization/Resources';
import { ErrorHandler } from '../Error/ErrorHandler';
import { LevelRequest } from '../Levels/LevelRequest';

@provide(BannedLayoutMiddleware)
export class BannedLayoutMiddleware {
    constructor(
        @inject(AuthorizationService) private authorizationService: AuthorizationService,
        @inject(ErrorHandler) private errorHandler: ErrorHandler
    ) {
    }

    public hasCreateAuthorization = (req: LevelRequest<void>, res: Response, next: () => void) => {
        let authorized = this.authorizationService.canCreate(Resources.BannedLayout, req.user);

        if (!authorized) {
            return this.errorHandler.sendForbiddenErrorResponse(res);
        }

        next();
    }
}
