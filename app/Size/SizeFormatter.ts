import { provide } from 'inversify-binding-decorators';

@provide(SizeFormatter)
export class SizeFormatter {
    formatSize(size: number) {
        return `${size}x${size}`;
    }
}
