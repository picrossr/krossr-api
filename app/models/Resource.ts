import { Resources } from '../Authorization/Resources';

export interface Resource {
    resource: Resources;
}
