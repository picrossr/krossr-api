import { injectable } from 'inversify';
import { DataTypes, Sequelize } from 'sequelize';
import { Model } from 'sequelize';
import { Resources } from '../Authorization/Resources';
import { TableNames } from '../Database/TableNames';
import { ModelConfiguration } from './ModelConfiguration';
import { Resource } from './Resource';

export interface BannedLayoutCreationAttributes {
    layout: string;
    message?: string;
}

export interface BannedLayoutAttributes extends BannedLayoutCreationAttributes {
    id: number;
}

export class BannedLayout
    extends Model<BannedLayoutAttributes, BannedLayoutCreationAttributes> implements BannedLayoutAttributes, Resource {
        public id!: number;
        public layout!: string;
        public message: string;

        // not saved in db table
        public resource = Resources.BannedLayout;
    }

@injectable()
export class BannedLayoutConfiguration implements ModelConfiguration<Sequelize> {
    configure(sequelize: Sequelize): void {
        BannedLayout.init({
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            layout: {
                allowNull: false,
                type: DataTypes.STRING,
                unique: true
            },
            message: {
                allowNull: true,
                type: DataTypes.STRING
            }
        }, {
            tableName: TableNames.BannedLayouts,
            sequelize
        });
    }
}
