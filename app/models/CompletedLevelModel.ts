import { injectable } from 'inversify';
import { DataTypes, Model, Sequelize } from 'sequelize';
import { ModelConfiguration } from './ModelConfiguration';

export interface CompletedLevelCreationAttributes {
    userId: number;
    levelId: number;
}

interface CompletedLevelAttributes extends CompletedLevelCreationAttributes {
    id: number;
}

export class CompletedLevel extends Model<CompletedLevelAttributes, CompletedLevelCreationAttributes> {
    public id!: number;
    public userId!: number;
    public levelId!: number;
}

@injectable()
export class CompletedLevelConfiguration implements ModelConfiguration<Sequelize> {
    private compositeKey = 'oneCompletionPerLevelPerUser';

    configure(sequelize: Sequelize) {
        CompletedLevel.init({
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            userId: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                unique: this.compositeKey
            },
            levelId: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                unique: this.compositeKey
            }
        }, {
            tableName: 'completedLevels',
            sequelize
        });

    }
}
