import * as swig from 'swig';
import { provideSingleton } from '../Utils/provideSingleton';

@provideSingleton(HtmlRenderer)
export class HtmlRenderer {
    /* extracts html from a file, inserts variables, returns the completed string */
    render(url: string, options: any): string {
        return swig.renderFile(url, options);
    }
}
