import { Container } from 'inversify';
import { ErrorContainer } from '../../spec/containers/error-container';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { CompletedLevelRepository } from './CompletedLevelRepository';
import { CompletedLevelService } from './CompletedLevelService';

describe('CompletedLevelService', () => {
    let container: Container;
    let service: CompletedLevelService;

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);

        container.bind(CompletedLevelRepository).toSelf();
        container.bind(CompletedLevelService).toSelf();
        service = container.get(CompletedLevelService);
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });
});
