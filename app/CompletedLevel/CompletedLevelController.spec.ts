import { Container } from 'inversify';
import { ErrorContainer } from '../../spec/containers/error-container';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { CompletedLevelController } from './CompletedLevelController';
import { CompletedLevelRepository } from './CompletedLevelRepository';
import { CompletedLevelService } from './CompletedLevelService';

describe('CompletedLevelController', () => {
    let container: Container;
    let service: CompletedLevelController;

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(CompletedLevelRepository).toSelf();
        container.bind(CompletedLevelService).toSelf();
        container.bind(CompletedLevelController).toSelf();
        service = container.get(CompletedLevelController);
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });
});
