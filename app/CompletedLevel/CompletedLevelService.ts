import { inject } from 'inversify';
import { provide } from 'inversify-binding-decorators';
import { CompletedLevelCreationAttributes } from '../models/CompletedLevelModel';
import { Level } from '../models/LevelModel';
import { User } from '../models/UserModel';
import { CompletedLevelRepository } from './CompletedLevelRepository';

@provide(CompletedLevelService)
export class CompletedLevelService {
    constructor(
        @inject(CompletedLevelRepository) private completedLevelRepository: CompletedLevelRepository
    ) {
    }

    async complete(level: Level, user: User): Promise<void> {
        let attributes: CompletedLevelCreationAttributes = {
            userId: user.id,
            levelId: level.id
        };

        return await this.completedLevelRepository.upsert(attributes);
    }
}
