import { SignOutController } from './SignOutController';
import { Container } from 'inversify';
import { MockRequest } from '../../spec/support/MockRequest';

describe('SignOutController', () => {
    let container: Container;
    let controller: SignOutController;

    beforeEach(() => {
        container = new Container();
        container.bind(SignOutController).toSelf();
        controller = container.get(SignOutController);
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    it('should sign out', () => {
        let request = new MockRequest();
        spyOn(request, 'logout');

        controller.signOut(request as any);
        expect(request.logout).toHaveBeenCalled();
    });
});
