import { inject } from 'inversify';
import { User, UserCreationAttributes } from '../models/UserModel';
import { UserViewModelMapper } from './UserViewModelMapper';
import { Op } from 'sequelize';
import { UserRepository } from './UserRepository';
import { provide } from 'inversify-binding-decorators';
import { UserViewModel } from './UserViewModel';

@provide(UserService)
export class UserService {
    constructor(
        @inject(UserViewModelMapper) private userViewModelMapper: UserViewModelMapper,
        @inject(UserRepository) private userRepository: UserRepository
    ) {
    }

    createUserModel(attributes: UserCreationAttributes) {
        return User.build(attributes);
    }

    async getUser(id: number): Promise<UserViewModel> {
        return this.userViewModelMapper.toViewModel(await this.getUserModel(id));
    }

    async getUserByUsername(username: string): Promise<UserViewModel> {
        return this.userViewModelMapper.toViewModel(await this.getUserModelByUsername(username));
    }

    async getUserByToken(token: string) {
        return await User.findOne({
            where: {
                resetPasswordToken: token,
                resetPasswordExpires: {
                    [Op.gt]: new Date()
                }
            }
        });
    }

    async getUserModel(id: number): Promise<User> {
        let model = await User.findOne({
            where: { id }
        });

        return model;
    }

    async getUserModelByUsername(username: string): Promise<User> {
        let model = await User.findOne({
            where: { username }
        });

        return model;
    }

    async saveUser(user: User): Promise<User> {
        return await this.userRepository.saveUser(user);
    }

    async updateUser(user: User, keys: object) {
        return await this.userRepository.updateUser(user, keys);
    }
}
