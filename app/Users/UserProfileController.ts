import { UserRequest } from './UserRequest';
import { UserViewModelMapper } from './UserViewModelMapper';
import { inject } from 'inversify';
import { UserService } from './UserService';
import { provide } from 'inversify-binding-decorators';
import { Body, Get, Put, Request, Res, Route, TsoaResponse } from 'tsoa';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { ErrorResponse } from '../Error/ErrorResponse';
import { UserViewModel } from './UserViewModel';

@provide(UserProfileController)
@Route('users')
export class UserProfileController {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(UserViewModelMapper) private userMapper: UserViewModelMapper,
        @inject(UserService) private userService: UserService
    ) {
    }

    /**
     * Update user details
     */
    @Put()
    public async updateUser(
        @Request() req: UserRequest,
        @Body() body: { email: string },
        @Res() notSignedInResponse: TsoaResponse<400, ErrorResponse>,
        @Res() unknownServerErrorResponse: TsoaResponse<500, ErrorResponse>
    ): Promise<UserViewModel> {
        // Init Variables
        let user = req.user;

        if (!user) {
            notSignedInResponse(400, { message: 'User is not signed in' });
        }

        try {
            await this.userService.updateUser(user, {
                email: body.email
            });

            return new Promise((resolve, reject) => {
                req.login(user, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        let result = this.userMapper.toViewModel(user);
                        resolve(result);
                    }
                });
            });
        } catch (err) {
            unknownServerErrorResponse(500, this.errorMessageService.getErrorResponse(err));
        }
    }

    /**
     * Send User
     */
    @Get('me')
    public async me(@Request() req: UserRequest) {
        if (req.user) {
            return this.userMapper.toViewModel(req.user);
        }

        return null;
    }
}
