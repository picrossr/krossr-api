/* istanbul ignore file */
/* we trust sequelize/whatever to do its job... */

import { provide } from 'inversify-binding-decorators';
import { User } from '../models/UserModel';

@provide(UserRepository)
export class UserRepository {
    async saveUser(user: User) {
        return await user.save();
    }

    async updateUser(user: User, keys: object) {
        return await user.update(keys);
    }
}
