import { UserProfileController } from './UserProfileController';
import { interfaces } from 'inversify';
import { ErrorHandler } from '../Error/ErrorHandler';
import { UserViewModelMapper } from './UserViewModelMapper';
import { UserService } from './UserService';
import { UserRepository } from './UserRepository';
import { User } from '../models/UserModel';
import { TestHelpers } from '../../spec/support/TestHelpers';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorContainer } from '../../spec/containers/error-container';

describe('UserProfileController', () => {
    let container: interfaces.Container;
    let controller: UserProfileController;
    let errorHandler: ErrorHandler;
    let userService: UserService;

    let notSignedInResponse: TsoaResponse<400, ErrorResponse>;
    let unknownServerErrorResponse: TsoaResponse<500, ErrorResponse>;

    class MockUpdateUserRequest {
        body: { email: string }; // todo
        user: User;
        login?: (user: User, callback: (err: any) => void) => void;
    }

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(UserService).toSelf().inSingletonScope();
        container.bind(UserRepository).toSelf().inSingletonScope();
        container.bind(UserViewModelMapper).toSelf();
        container.bind(UserProfileController).toSelf();
        controller = container.get(UserProfileController);
        userService = container.get(UserService);
        errorHandler = container.get(ErrorHandler);

        notSignedInResponse = jasmine.createSpy();
        unknownServerErrorResponse = jasmine.createSpy();
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    describe('#me', () => {
        it('should return a logged in user if one exists', async () => {
            let request = new MockUpdateUserRequest();
            request.user = userService.createUserModel(TestHelpers.getTestUserAttributes());
            request.user.id = 1;

            let response = await controller.me(request as any);
            expect(response).toEqual({ id: 1, username: 'rosalyn' });
        });

        it('should return null if none exists', async () => {
            let request = new MockUpdateUserRequest();
            let response = await controller.me(request as any);

            expect(response).toEqual(null);
        });
    });

    describe('#update', () => {
        it('should detect if the user is not signed in', async () => {
            spyOn(errorHandler, 'sendClientErrorResponse');
            let request = new MockUpdateUserRequest();

            await controller.updateUser(request as any, request.body, notSignedInResponse, unknownServerErrorResponse);
            expect(notSignedInResponse).toHaveBeenCalled();
        });

        it('should update the user if signed in', async () => {
            let request = new MockUpdateUserRequest();
            request.body = { email: 'change@to.me' };
            request.user = userService.createUserModel(TestHelpers.getTestUserAttributes());
            request.login = (user, callback) => callback(null);

            spyOn(userService, 'updateUser').and.returnValue(Promise.resolve(null));

            await controller.updateUser(request as any, request.body, notSignedInResponse,  unknownServerErrorResponse);
            expect(userService.updateUser).toHaveBeenCalled();
        });

        it('should catch an update error', async () => {
            let request = new MockUpdateUserRequest();
            request.body = { email: 'change@to.me' };
            request.user = userService.createUserModel(TestHelpers.getTestUserAttributes());
            request.login = (user, callback) => callback(null);

            spyOn(userService, 'updateUser').and.throwError('nope');

            await controller.updateUser(request as any, request.body, notSignedInResponse, unknownServerErrorResponse);
            expect(unknownServerErrorResponse).toHaveBeenCalled();
        });

        it('should catch a login error', async () => {
            let request = new MockUpdateUserRequest();
            request.body = { email: 'change@to.me' };
            request.user = userService.createUserModel(TestHelpers.getTestUserAttributes());
            request.login = (user, callback) => callback('error');

            spyOn(userService, 'updateUser').and.returnValue(Promise.resolve(null));

            let fn = async () => await controller.updateUser(request as any, request.body, notSignedInResponse, unknownServerErrorResponse);
            await expectAsync(fn()).toBeRejectedWith('error');
        });
    });
});
