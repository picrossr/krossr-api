import { ViewModelMapper } from '../ViewModel/ViewModelMapper';
import { User } from '../models/UserModel';
import { provide } from 'inversify-binding-decorators';
import { UserViewModel } from './UserViewModel';

@provide(UserViewModelMapper)
export class UserViewModelMapper implements ViewModelMapper<User, UserViewModel> {
    toViewModel(model: User): UserViewModel {
        if (!model) {
            return null;
        }

        return {
            id: model.id,
            username: model.username
        };
    }
}
