import { provide } from 'inversify-binding-decorators';
import { Post, Request, Route } from 'tsoa';

@provide(SignOutController)
@Route('auth')
export class SignOutController {
    @Post('signout')
    public async signOut(@Request() req: Express.Request)  {
        req.logout();
        return;
    }
}
