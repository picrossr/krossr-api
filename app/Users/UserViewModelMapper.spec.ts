import { UserViewModelMapper } from './UserViewModelMapper';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { User } from '../models/UserModel';
import { Roles } from '../Authorization/Roles';

describe('UserViewModelMapper', () => {
    let mapper: UserViewModelMapper;

    function getUser() {
        return User.build({
            salt: 'mmmsalty',
            hashedPassword: 'hashSlingingSlasher!',
            email: 'fake@fake.com',
            provider: 'local',
            username: 'fake',
            role: Roles.User
        });
    }

    beforeEach(async () => {
        let container = await new SequelizeContainer().initialize();
        container.bind(UserViewModelMapper).toSelf();
        mapper = container.get(UserViewModelMapper);
    });

    it('should exist', () => {
        expect(mapper).toBeTruthy();
    });

    it('should map correctly', () => {
        let user = getUser();
        user.id = 1;

        expect(mapper.toViewModel(user)).toEqual({ id: 1, username: 'fake' });
    });
});
