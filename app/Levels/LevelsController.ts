import { LevelRequest } from './LevelRequest';
import { inject } from 'inversify';
import { LevelService } from './LevelService';
import { provide } from 'inversify-binding-decorators';
import { Body, Delete, Get, Path, Post, Put, Request, Res, Route, Security, TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { Securities } from '../Authorization/Securities';
import { Resources } from '../Authorization/Resources';
import { CreateLevelBodyViewModel } from './CreateLevelBodyViewModel';
import { UpdateLevelBodyViewModel } from './UpdateLevelBodyViewModel';

@provide(LevelsController)
@Route('levels')
export class LevelsController {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(LevelService) private levelService: LevelService
    ) {
    }

    /**
     * Create a Level
     */
    @Post()
    @Security(Securities.RequiresLogin)
    public async createLevel(
        @Request() req: LevelRequest<CreateLevelBodyViewModel>,
        @Body() body: CreateLevelBodyViewModel,
        @Res() serverErrorResponse: TsoaResponse<500, ErrorResponse>
    ) {
        try {
            let level = await this.levelService.createLevel(req.user, body);

            if (level) {
                return level;
            } else {
                serverErrorResponse(500, { message: 'There was a problem creating the level' });
            }
        } catch (err) {
            serverErrorResponse(500, this.errorMessageService.getErrorResponse(err));
        }
    }

    /**
     * Delete a Level
     */
    @Delete('{levelId}')
    @Security(Securities.RequiresLogin)
    @Security(Securities.CanDelete, [Resources.Level])
    public async deleteLevel(
        @Path() levelId: string,
        @Request() req: LevelRequest<void>,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ) {
        try {
            await this.levelService.deleteLevel(req.level);
            return;
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }

    /**
     * Show the current Level.
     */
    @Get('{levelId}')
    public async readLevel(
        @Path() levelId: string,
        @Request() req: LevelRequest<void>,
        @Res() unknownServerErrorResponse: TsoaResponse<500, ErrorResponse>
    ) {
        try {
            return await this.levelService.getLevelById(req.user, levelId);
        } catch (err) {
            unknownServerErrorResponse(500, this.errorMessageService.getErrorResponse(err));
        }
    }

    @Put('{levelId}')
    @Security(Securities.RequiresLogin)
    @Security(Securities.CanUpdate, [Resources.Level])
    public async updateLevel(
        @Path() levelId: string,
        @Body() body: UpdateLevelBodyViewModel,
        @Request() req: LevelRequest<UpdateLevelBodyViewModel>,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ) {
        try {
            return await this.levelService.updateLevel(levelId, req.user, body);
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }
}
