import { Container } from 'inversify';
import { LevelsController } from './LevelsController';
import { ErrorHandler } from '../Error/ErrorHandler';
import { LevelService } from './LevelService';
import { LevelViewModelMapper } from './LevelViewModelMapper';
import { LevelRepository } from './LevelRepository';
import { MockResponse } from '../../spec/support/MockResponse';
import { TestHelpers } from '../../spec/support/TestHelpers';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { BannedLayoutService } from '../BannedLayout/BannedLayoutService';
import { BannedLayoutRepository } from '../BannedLayout/BannedLayoutRepository';
import { ErrorContainer } from '../../spec/containers/error-container';
import { UpdateLevelBodyViewModel } from './UpdateLevelBodyViewModel';
import { CreateLevelBodyViewModel } from './CreateLevelBodyViewModel';
import { UserViewModel } from '../Users/UserViewModel';

describe('LevelsController', () => {
    let container: Container;
    let controller: LevelsController;
    let errorHandler: ErrorHandler;
    let levelService: LevelService;

    class MockCreateLevelRequest {
        body: CreateLevelBodyViewModel;
        user: UserViewModel;
    }

    class MockUpdateLevelRequest {
        body: UpdateLevelBodyViewModel;
        user: UserViewModel;

        params: { levelId: string };

        constructor(levelId: number) {
            this.params = {
                levelId: levelId.toString()
            };
        }
    }

    class MockDeleteLevelRequest {
        params: { levelId: string };

        constructor(levelId: number) {
            this.params = {
                levelId: levelId.toString()
            };
        }
    }

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(BannedLayoutRepository).toSelf();
        container.bind(BannedLayoutService).toSelf();
        container.bind(LevelViewModelMapper).toSelf();
        container.bind(LevelService).toSelf().inSingletonScope();
        container.bind(LevelRepository).toSelf();
        container.bind(LevelsController).toSelf();
        controller = container.get(LevelsController);
        errorHandler = container.get(ErrorHandler);
        levelService = container.get(LevelService);
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    describe('#create', () => {
        it('should create a level', async () => {
            spyOn(levelService, 'createLevel').and.returnValue(Promise.resolve(
                levelService.createLevelModel(TestHelpers.getTestLevelAttributes(1))
            ));

            let request = new MockCreateLevelRequest();
            let errorResponse = jasmine.createSpy();

            await controller.createLevel(request as any, request.body, errorResponse);
            expect(levelService.createLevel).toHaveBeenCalled();
        });

        it('should handle a known service failure', async () => {
            spyOn(errorHandler, 'sendServerErrorResponse');
            spyOn(levelService, 'createLevel').and.returnValue(Promise.resolve(null));

            let request = new MockCreateLevelRequest();
            let errorResponse = jasmine.createSpy();

            await controller.createLevel(request as any, request.body, errorResponse);
            expect(errorResponse).toHaveBeenCalled();
        });

        it('should handle an unknown service failure', async () => {
            spyOn(errorHandler, 'sendUnknownServerErrorResponse');
            spyOn(levelService, 'createLevel').and.throwError('poop');

            let request = new MockCreateLevelRequest();
            let errorResponse = jasmine.createSpy();

            await controller.createLevel(request as any, request.body, errorResponse);
            expect(errorResponse).toHaveBeenCalled();
        });
    });

    describe('#delete', () => {
        it('should delete a level', async () => {
            spyOn(levelService, 'deleteLevel').and.returnValue(Promise.resolve());

            let request = new MockDeleteLevelRequest(1);

            let response = new MockResponse();

            await controller.deleteLevel(request.params.levelId, request as any, response as any);
        });

        it('should handle an unknown service failure', async () => {
            spyOn(errorHandler, 'sendUnknownServerErrorResponse');
            spyOn(levelService, 'deleteLevel').and.throwError('dang');

            let request = new MockDeleteLevelRequest(1);
            let error = jasmine.createSpy();

            await controller.deleteLevel(request.params.levelId, request as any, error);
            expect(error).toHaveBeenCalled();
        });
    });

    describe('#read', () => {
        it('should read a level', async () => {
            let request = new MockDeleteLevelRequest(1);
            let errorResponse = jasmine.createSpy();
            spyOn(levelService, 'getLevelById').and.returnValue(Promise.resolve(TestHelpers.getTestLevelAttributes(1)));

            expect(await controller.readLevel(request.params.levelId, request as any, errorResponse)).toBeTruthy();
        });

        it('should handle an unknown service failure', async () => {
            spyOn(errorHandler, 'sendUnknownServerErrorResponse');
            spyOn(levelService, 'getLevel').and.throwError('dang');

            let request = new MockDeleteLevelRequest(1);

            let errorResponse = jasmine.createSpy();

            await controller.readLevel(request.params.levelId, request as any, errorResponse);
            expect(errorResponse).toHaveBeenCalled();
        });
    });

    describe('#update', () => {
        it('should update a level', async () => {
            spyOn(levelService, 'updateLevel').and.returnValue(Promise.resolve(TestHelpers.getTestLevelAttributes(1)));
            let request = new MockUpdateLevelRequest(1);
            let serverError = jasmine.createSpy();

            await controller.updateLevel(request.params.levelId, request.body, request as any, serverError);
            expect(levelService.updateLevel).toHaveBeenCalled();
        });

        it('should handle an unknown service failure', async () => {
            spyOn(errorHandler, 'sendUnknownServerErrorResponse');
            spyOn(levelService, 'updateLevel').and.throwError('dang');

            let request = new MockUpdateLevelRequest(1);

            let serverError = jasmine.createSpy();

            await controller.updateLevel(request.params.levelId, request.body, request as any, serverError);
            expect(serverError).toHaveBeenCalled();
        });
    });
});
