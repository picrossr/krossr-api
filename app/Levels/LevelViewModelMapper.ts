import { ViewModelMapper } from '../ViewModel/ViewModelMapper';
import { Level } from '../models/LevelModel';
import { inject } from 'inversify';
import { User } from '../models/UserModel';
import { AuthorizationService } from '../Authorization/AuthorizationService';
import { Resources } from '../Authorization/Resources';
import { provide } from 'inversify-binding-decorators';
import { LevelViewModel } from './LevelViewModel';

@provide(LevelViewModelMapper)
export class LevelViewModelMapper implements ViewModelMapper<Level, LevelViewModel> {
    constructor(
        @inject(AuthorizationService) private authorizationService: AuthorizationService
    ) {
    }

    toViewModel(model: Level, user: User): LevelViewModel {
        let canDelete = this.authorizationService.canDelete(model, user);
        let canBanLayout = this.authorizationService.canCreate(Resources.BannedLayout, user);

        return {
            id: model.id,
            layout: model.layout,
            name: model.name,
            size: model.size,
            canBanLayout,
            canDelete
        };
    }
}
