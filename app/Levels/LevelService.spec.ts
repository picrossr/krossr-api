import { LevelService } from './LevelService';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { LevelViewModelMapper } from './LevelViewModelMapper';
import { LevelRepository } from './LevelRepository';
import { UserService } from '../Users/UserService';
import { TestHelpers } from '../../spec/support/TestHelpers';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { Rating } from '../models/RatingModel';
import { UserRepository } from '../Users/UserRepository';
import { BannedLayoutService } from '../BannedLayout/BannedLayoutService';
import { BannedLayoutRepository } from '../BannedLayout/BannedLayoutRepository';

describe('LevelService', () => {
    let service: LevelService;
    let repository: LevelRepository;
    let userService: UserService;

    beforeEach(async () => {
        let DIContainer = await new SequelizeContainer().initialize();

        DIContainer.bind(BannedLayoutService).toSelf();
        DIContainer.bind(BannedLayoutRepository).toSelf();
        DIContainer.bind(LevelRepository).toSelf().inSingletonScope();
        DIContainer.bind(LevelViewModelMapper).toSelf();
        DIContainer.bind(LevelService).toSelf();
        DIContainer.bind(UserViewModelMapper).toSelf();
        DIContainer.bind(UserService).toSelf().inSingletonScope();
        DIContainer.bind(UserRepository).toSelf().inSingletonScope();
        service = DIContainer.get(LevelService);
        repository = DIContainer.get(LevelRepository);
        userService = DIContainer.get(UserService);
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });

    it('should create a level', async () => {
        let user = userService.createUserModel(TestHelpers.getTestUserAttributes());
        user.id = 1;

        let model = service.createLevelModel(TestHelpers.getTestLevelAttributes(user.id));
        spyOn(repository, 'createLevel').and.returnValue(Promise.resolve(model));

        await service.createLevel(user, {
            name: 'wat',
            decodedLayout: [
                [true, false, false, false, true],
                [true, false, false, false, true],
                [true, false, false, false, true],
                [true, false, false, false, true],
                [true, true, true, true, true]
            ]
        });

        expect(repository.createLevel).toHaveBeenCalled();
    });

    it('should delete a level', async () => {
        let model = service.createLevelModel(TestHelpers.getTestLevelAttributes(1));
        spyOn(repository, 'deleteLevel');

        await service.deleteLevel(model);

        expect(repository.deleteLevel).toHaveBeenCalled();
    });

    it('should read a level', async () => {
        let user = userService.createUserModel(TestHelpers.getTestUserAttributes());
        let model = service.createLevelModel(TestHelpers.getTestLevelAttributes(1));

        let result = await service.getLevel(user, model);
        expect(result).toBeTruthy();
    });

    it('should read a level and include the users own rating, if available', async () => {
        let user = userService.createUserModel(TestHelpers.getTestUserAttributes());
        user.id = 1;

        let model = service.createLevelModel(TestHelpers.getTestLevelAttributes(1));
        let rating = Rating.build({ rating: 5, userId: 1, levelId: model.id });
        model.ratings = [rating];

        let result = await service.getLevel(user, model);
        expect(result.userRating).toEqual(rating);
    });

    it('should update a level', async () => {
        let user = userService.createUserModel(TestHelpers.getTestUserAttributes());
        user.id = 1;

        let model = service.createLevelModel(TestHelpers.getTestLevelAttributes(1));
        model.id = 1;
        spyOn(repository, 'updateLevel').and.returnValue(Promise.resolve(model));

        await service.updateLevel(model.id.toString(), user, {
            id: model.id,
            name: 'wat',
            decodedLayout: [
                [true, false, true, false, true],
                [true, false, false, false, true],
                [true, false, false, false, true],
                [true, false, false, false, true],
                [true, true, true, true, true]
            ]
        });

        expect(repository.updateLevel).toHaveBeenCalled();
    });
});
