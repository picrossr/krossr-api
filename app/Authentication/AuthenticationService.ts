/* istanbul ignore file */
/** this file is ignored bc we trust passport & promise to do their jobs, and should be mocked in other tests */

import { User } from '../models/UserModel';
import * as passport from 'passport';
import { provide } from 'inversify-binding-decorators';
import { SignInBodyViewModel } from '../SignIn/SignInBodyViewModel';

@provide(AuthenticationService)
export class AuthenticationService {
    authenticate(req: SignInBodyViewModel): Promise<User> {
        return new Promise((resolve, reject) => {
            passport.authenticate('local', (error, user: User, info) => {
                if (error) {
                    reject(error);
                }

                if (!user) {
                    reject({ message: 'Incorrect username or password' });
                }

                resolve(user);
            })({ body: req });
        });
    }
}
