import { LevelListService } from './LevelListService';
import { LevelListLevelViewModelMapper } from './LevelListLevelViewModelMapper';
import { inject } from 'inversify';
import { provide } from 'inversify-binding-decorators';
import { Get, Query, Request, Res, Route, TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { LevelListQuery } from './LevelListQuery';
import { KrossrRequest } from '../KrossrRequest/KrossrRequest';
import { LevelListViewModel } from './LevelListViewModel';
import { LevelListFilterOptions } from './LevelListFilterOptions';

@provide(LevelListController)
// todo this is gonna need to change to avoid conflicting with normal levels route
// when we no longer have any control over order of registration (rest of tsoa stuff done)
@Route('levels')
export class LevelListController {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(LevelListLevelViewModelMapper) private levelListMapper: LevelListLevelViewModelMapper,
        @inject(LevelListService) private levelListService: LevelListService,
    ) {
    }

    @Get('options')
    public async getLevelListOptions() {
        return this.levelListService.getOptions();
    }

    /*
     * this really only exists to force openapi definitions to exist for
     * the LevelListFilterOptions interface. todo better way?
     */
    @Get('defaultFilterOptions')
    public async getDefaultFilterOptions() {
        return {} as LevelListFilterOptions;
    }

    @Get()
    public async paginate(
        @Request() req: KrossrRequest,
        @Res() unknownServerErrorResponse: TsoaResponse<500, ErrorResponse>,
        @Query() pageNum: string,
        @Query() sizeRestriction?: string,
        @Query() searchText?: string,
        @Query() sortBy?: string,
        @Query() sortDirection?: string
    ) {
        let query: LevelListQuery = {
            numPerPage: '9',
            pageNum,
            sizeRestriction,
            searchText,
            sortBy,
            sortDirection
        };

        let numPerPage = parseInt(query.numPerPage, 10);

        try {
            let levels = await this.levelListService.getList(query, req.user);
            let rows = levels.rows.map(level => this.levelListMapper.toViewModel(level, req.user));

            return {
                levels: rows,
                count: levels.count,
                numPerPage
            } as LevelListViewModel;
        } catch (err) {
            unknownServerErrorResponse(500, this.errorMessageService.getErrorResponse(err));
        }
    }
}
