import { SizeFormatter } from '../Size/SizeFormatter';
import { LevelListService } from './LevelListService';
import { LevelListRepository } from './LevelListRepository';
import { User } from '../models/UserModel';
import { Roles } from '../Authorization/Roles';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';

describe('LevelListService', () => {
    let service: LevelListService;
    let repo: LevelListRepository;

    function getUser() {
        return User.build({
            salt: 'mmmsalty',
            hashedPassword: 'hashSlingingSlasher!',
            email: 'fake@fake.com',
            provider: 'local',
            username: 'fake',
            role: Roles.User
        });
    }

    beforeEach(async () => {
        let DIContainer = await new SequelizeContainer().initialize();
        DIContainer.bind(SizeFormatter).toSelf();
        DIContainer.bind(LevelListService).toSelf().inSingletonScope();
        DIContainer.bind(LevelListRepository).toSelf().inSingletonScope();
        service = DIContainer.get(LevelListService);
        repo = DIContainer.get(LevelListRepository);
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });

    it('should return options correctly', () => {
        let options = service.getOptions();

        expect(options.sizeOptions).toEqual({ All: null, '5x5': 5, '10x10': 10, '15x15': 15 });
    });

    it('should get list', () => {
        spyOn(repo, 'getList');
        service.getList({ pageNum: '0', numPerPage: '9' }, getUser());
        expect(repo.getList).toHaveBeenCalled();
    });
});
