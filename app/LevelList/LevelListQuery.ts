import { LevelListFilterOptions } from './LevelListFilterOptions';

export interface LevelListQuery extends LevelListFilterOptions {
    pageNum: string;
    numPerPage: string;
}
