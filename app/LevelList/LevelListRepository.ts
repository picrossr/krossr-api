/* istanbul ignore file */

import { provide } from 'inversify-binding-decorators';
import { Sequelize, Op } from 'sequelize';
import { Level } from '../models/LevelModel';
import { User } from '../models/UserModel';
import { LevelListQuery } from './LevelListQuery';
import { LevelListSortByOptions } from './LevelListSortByOptions';

@provide(LevelListRepository)
export class LevelListRepository {
    getList(query: LevelListQuery, user: User) {
        let pageNum = parseInt(query.pageNum, 10);
        let sizeRestriction = query.sizeRestriction;
        let searchText = query.searchText;
        let sortBy = query.sortBy || LevelListSortByOptions.CreatedDate;
        let sortDirection = query.sortDirection || 'ASC';
        let numPerPage = parseInt(query.numPerPage, 10);

        let whereBuilder: any = {
            id: {
                [Op.not]: null
            }
        };

        /**
         * This may be able to be done better, but hey. Include the average rating of a level with the levels query,
         * rather than every ratings object for that level.
         * Also, exclude levels without average ratings if ratings are being filtered upon
         */
        let baseRatingQuery = '(SELECT AVG("ratings"."rating") FROM "ratings" WHERE "ratings"."levelId" = "Level"."id")';
        let ratingQuery = Sequelize.literal(baseRatingQuery);
        let ratingTest = Sequelize.literal(baseRatingQuery + ' IS NOT NULL');

        let isRating = (sortBy === LevelListSortByOptions.Ratings);

        if (sizeRestriction) {
            whereBuilder.size = {
                [Op.eq]: sizeRestriction
            };
        }

        if (searchText) {
            searchText = '%' + searchText + '%';

            whereBuilder[Op.or] = [
                {
                    name: {
                        [Op.iLike]: searchText
                    }
                },
                Sequelize.where(Sequelize.col('user.username'), 'ILIKE', searchText)
            ];
        }

        let userId = user ? user.id : null;

        // user is sanitized bc middleware uses user input to pull user from db and resubmits our copy to here
        let baseCompletedQuery =
            `
                (
                    SELECT EXISTS
                    (
                        SELECT true
                        FROM "completedLevels"
                        WHERE "completedLevels"."levelId" = "Level"."id"
                        AND "completedLevels"."userId" = ${userId}
                    )
                )
            `;

        let completedQuery = Sequelize.literal(baseCompletedQuery);

        return Level.findAndCountAll({
            include: [
                {
                    association: Level.associations.user,
                    attributes: [nameof<User>(o => o.id), nameof<User>(o => o.username)],
                }
            ],
            attributes: {
                include: [
                    [ratingQuery, nameof<Level>(o => o.avgRating)],
                    [completedQuery, nameof<Level>(o => o.completed)]
                ]
            },
            where: {
                [Op.and]: isRating ? [whereBuilder, ratingTest] : whereBuilder
            },
            limit: numPerPage,
            offset: pageNum * numPerPage,
            order: Sequelize.literal(`${sortBy} ${sortDirection}`)
        });
    }
}
