export interface LevelListFilterSelectOptionsViewModel {
    sortByOptions: { [key: string]: string };
    sizeOptions: { [key: string]: number };
    sortDirectionOptions: { [key: string]: string };
}
