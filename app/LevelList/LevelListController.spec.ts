import { Container } from 'inversify';
import { LevelListController } from './LevelListController';
import { ErrorHandler } from '../Error/ErrorHandler';
import { LevelListLevelViewModelMapper } from './LevelListLevelViewModelMapper';
import { LevelListService } from './LevelListService';
import { SizeFormatter } from '../Size/SizeFormatter';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { LevelListQuery } from './LevelListQuery';
import { LevelListRepository } from './LevelListRepository';
import { LevelService } from '../Levels/LevelService';
import { LevelViewModelMapper } from '../Levels/LevelViewModelMapper';
import { LevelRepository } from '../Levels/LevelRepository';
import { UserService } from '../Users/UserService';
import { UserRepository } from '../Users/UserRepository';
import { TestHelpers } from '../../spec/support/TestHelpers';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { User } from '../models/UserModel';
import { BannedLayoutService } from '../BannedLayout/BannedLayoutService';
import { BannedLayoutRepository } from '../BannedLayout/BannedLayoutRepository';
import { ErrorContainer } from '../../spec/containers/error-container';

describe('LevelListController', () => {
    let container: Container;
    let controller: LevelListController;
    let errorHandler: ErrorHandler;
    let levelListService: LevelListService;
    let levelService: LevelService;
    let userService: UserService;

    class MockLevelListRequest {
        query: LevelListQuery;
        user: User;
    }

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(BannedLayoutService).toSelf();
        container.bind(BannedLayoutRepository).toSelf();
        container.bind(LevelListLevelViewModelMapper).toSelf();
        container.bind(LevelListService).toSelf().inSingletonScope();
        container.bind(LevelListController).toSelf();
        container.bind(LevelListRepository).toSelf().inSingletonScope();
        container.bind(SizeFormatter).toSelf();
        container.bind(UserViewModelMapper).toSelf();
        container.bind(LevelService).toSelf();
        container.bind(UserService).toSelf();
        container.bind(LevelViewModelMapper).toSelf();
        container.bind(LevelRepository).toSelf();
        container.bind(UserRepository).toSelf();
        controller = container.get(LevelListController);
        errorHandler = container.get(ErrorHandler);
        levelListService = container.get(LevelListService);
        levelService = container.get(LevelService);
        userService = container.get(UserService);
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    it('should get options', () => {
        expect(controller.getLevelListOptions()).toBeTruthy();
    });

    it('should get list', async () => {
        let levelModel = levelService.createLevelModel(TestHelpers.getTestLevelAttributes(1));
        let userModel = userService.createUserModel(TestHelpers.getTestUserAttributes());

        levelModel.user = userModel;

        spyOn(levelListService, 'getList').and.returnValue(Promise.resolve({ rows: [
            levelModel
        ], count: 0 }));

        let request = new MockLevelListRequest();
        request.query = { pageNum: '0', numPerPage: '9' };
        request.user = userModel;
        let errorResponse = jasmine.createSpy();

        expect(await controller.paginate(
            request as any,
            errorResponse,
            request.query.pageNum,
            request.query.sizeRestriction,
            null, null, null
        )).toBeTruthy();
    });

    it('should handle an unknown error', async () => {
        spyOn(errorHandler, 'sendUnknownServerErrorResponse');
        spyOn(levelListService, 'getList').and.throwError('what list?');

        let request = new MockLevelListRequest();
        request.query = { pageNum: '0', numPerPage: '9' };
        let errorResponse = jasmine.createSpy();

        await controller.paginate(
            request as any,
            errorResponse,
            request.query.pageNum,
            request.query.sizeRestriction,
            null, null, null
        );

        expect(errorResponse).toHaveBeenCalled();
    });
});
