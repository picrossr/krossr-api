import { PagedResponse } from '../PagedResponse/PagedResponse';
import { LevelListLevelViewModel } from './LevelListLevelViewModel';

export interface LevelListViewModel extends PagedResponse {
    levels: LevelListLevelViewModel[];
}
