import { KrossrRequest } from '../KrossrRequest/KrossrRequest';
import { CreateUserBodyViewModel } from './CreateUserBodyViewModel';

export interface SignUpRequest extends KrossrRequest<any, CreateUserBodyViewModel> {
}
