import { SignUpController } from './SignUpController';
import { Container } from 'inversify';
import { PasswordService } from '../Password/PasswordService';
import { ErrorHandler } from '../Error/ErrorHandler';
import { MockResponse } from '../../spec/support/MockResponse';
import { UserService } from '../Users/UserService';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { User } from '../models/UserModel';
import { SignUpService } from './SignUpService';
import { TestHelpers } from '../../spec/support/TestHelpers';
import { UserRepository } from '../Users/UserRepository';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { ErrorContainer } from '../../spec/containers/error-container';
import { CreateUserBodyViewModel } from './CreateUserBodyViewModel';
import { UserViewModel } from '../Users/UserViewModel';

describe('SignUpController', () => {
    let container: Container;
    let controller: SignUpController;
    let errorHandler: ErrorHandler;
    let passwordService: PasswordService;
    let userService: UserService;
    let signUpService: SignUpService;

    class MockSignUpRequest {
        body: CreateUserBodyViewModel;
        user?: UserViewModel;
        login?: (user: User, callback: (err: any) => void) => void;
    }

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(SignUpService).toSelf().inSingletonScope();
        container.bind(PasswordService).toSelf().inSingletonScope();
        container.bind(SignUpController).toSelf();
        container.bind(UserService).toSelf().inSingletonScope();
        container.bind(UserRepository).toSelf().inSingletonScope();
        container.bind(UserViewModelMapper).toSelf();
        controller = container.get(SignUpController);
        errorHandler = container.get(ErrorHandler);
        userService = container.get(UserService);
        passwordService = container.get(PasswordService);
        signUpService = container.get(SignUpService);
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    it('should sign up', () => {
        let request = new MockSignUpRequest();
        request.login = (user, callback) => callback(null);
        let response = new MockResponse();

        spyOn(signUpService, 'signUp').and.returnValue(
            Promise.resolve(userService.createUserModel(TestHelpers.getTestUserAttributes()))
        );

        controller.signUp(request as any, request.body, response as any);
        expect(signUpService.signUp).toHaveBeenCalled();
    });

    it('should handle login error', async () => {
        let request = new MockSignUpRequest();
        request.login = (user, callback) => callback('boop');
        let response = new MockResponse();

        spyOn(errorHandler, 'sendUnknownServerErrorResponse');
        spyOn(signUpService, 'signUp').and.returnValue(
            Promise.resolve(userService.createUserModel(TestHelpers.getTestUserAttributes()))
        );

        let fn = async () => await controller.signUp(request as any, request.body, response as any);
        await expectAsync(fn()).toBeRejected();
    });
});
