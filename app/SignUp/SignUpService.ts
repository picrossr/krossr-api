import { UserCreationAttributes, User } from '../models/UserModel';
import { inject } from 'inversify';
import { PasswordService } from '../Password/PasswordService';
import { UserService } from '../Users/UserService';
import { Roles } from '../Authorization/Roles';
import { provide } from 'inversify-binding-decorators';
import { CreateUserBodyViewModel } from './CreateUserBodyViewModel';

@provide(SignUpService)
export class SignUpService {
    constructor(
        @inject(PasswordService) private passwordService: PasswordService,
        @inject(UserService) private userService: UserService
    ) {
    }

    async signUp(params: CreateUserBodyViewModel): Promise<User> {
        let attributes: UserCreationAttributes = {
            email: params.email,
            username: params.username,
            provider: '',
            salt: '',
            hashedPassword: '',
            role: Roles.User
        };

        let user = this.userService.createUserModel(attributes);
        this.passwordService.setPassword(user, params.password);

        return await this.userService.saveUser(user);
    }
}
