import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { inject } from 'inversify';
import { SignUpRequest } from './SignUpRequest';
import { SignUpService } from './SignUpService';
import { provide } from 'inversify-binding-decorators';
import { Body, Post, Request, Res, Route, TsoaResponse } from 'tsoa';
import { ErrorResponse } from '../Error/ErrorResponse';
import { ErrorMessageService } from '../Error/ErrorMessageService';
import { UserViewModel } from '../Users/UserViewModel';
import { CreateUserBodyViewModel } from './CreateUserBodyViewModel';

@provide(SignUpController)
@Route('auth')
export class SignUpController {
    constructor(
        @inject(ErrorMessageService) private errorMessageService: ErrorMessageService,
        @inject(SignUpService) private signUpService: SignUpService,
        @inject(UserViewModelMapper) private userMapper: UserViewModelMapper
    ) {
    }

    @Post('signup')
    public async signUp(
        @Request() req: SignUpRequest,
        @Body() body: CreateUserBodyViewModel,
        @Res() serverError: TsoaResponse<500, ErrorResponse>
    ): Promise<UserViewModel> {
        try {
            let user = await this.signUpService.signUp(body);

            return new Promise((resolve, reject) => {
                req.login(user, (err) => {
                    if (err) {
                        reject(err);
                    }

                    let result = this.userMapper.toViewModel(user);
                    resolve(result);
                });
            });
        } catch (err) {
            serverError(500, this.errorMessageService.getErrorResponse(err));
        }
    }
}
