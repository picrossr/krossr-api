export interface CreateUserBodyViewModel {
    email: string;
    password: string;
    username: string;
}
