import { User } from '../models/UserModel';

export interface ViewModelMapper<TModel, TViewModel> {
    toViewModel(model: TModel, currentUser?: User): TViewModel;
}
