import { PasswordService } from './PasswordService';
import { User } from '../models/UserModel';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { Roles } from '../Authorization/Roles';

describe('PasswordService', () => {
    let service: PasswordService;

    function getUser() {
        return User.build({
            salt: 'mmmsalty',
            hashedPassword: 'hashSlingingSlasher!',
            email: 'fake@fake.com',
            provider: 'local',
            username: 'fake',
            role: Roles.User
        });
    }

    beforeEach(async () => {
        let container = await new SequelizeContainer().initialize();

        container.bind(PasswordService).toSelf();
        service = container.get(PasswordService);
    });

    it('should exist', () => {
        expect(service).toBeTruthy();
    });

    it('should authenticate a password against a hashed password', () => {
        let user = getUser();

        expect(service.authenticate(user, 'obviously false password')).toBeFalsy();

        service.setPassword(user, 'hashslingingslasher');
        expect(service.authenticate(user, 'hashslingingslasher')).toBeTruthy();
    });

    it('should not set a new password if the password is missing', () => {
        let user = getUser();

        service.setPassword(user, 'hashslingingslasher');
        expect(service.authenticate(user, 'hashslingingslasher')).toBeTruthy();

        service.setPassword(user, '');
        expect(service.authenticate(user, '')).toBeFalsy();
        expect(service.authenticate(user, 'hashslingingslasher')).toBeTruthy();
    });
});
