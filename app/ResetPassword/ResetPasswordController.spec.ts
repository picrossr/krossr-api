import { ResetPasswordController } from './ResetPasswordController';
import { Container } from 'inversify';
import { PasswordService } from '../Password/PasswordService';
import { ErrorHandler } from '../Error/ErrorHandler';
import { MockResponse } from '../../spec/support/MockResponse';
import { UserService } from '../Users/UserService';
import { UserViewModelMapper } from '../Users/UserViewModelMapper';
import { User } from '../models/UserModel';
import { IMailerService } from '../Mailer/IMailerService';
import { MailerSymbols } from '../Mailer/MailerSymbols';
import { MockMailerService } from '../../spec/support/MockMailerService';
import { SequelizeContainer } from '../../spec/containers/sequelize-container';
import { TestHelpers } from '../../spec/support/TestHelpers';
import { UserRepository } from '../Users/UserRepository';
import { MailerService } from '../Mailer/MailerService';
import { ErrorContainer } from '../../spec/containers/error-container';
import { HtmlRenderer } from '../HtmlRenderer/HtmlRenderer';
import { ResetPasswordBodyViewModel } from './ResetPasswordBodyViewModel';
import { UserViewModel } from '../Users/UserViewModel';

describe('ResetPasswordController', () => {
    let container: Container;
    let controller: ResetPasswordController;
    let errorHandler: ErrorHandler;
    let htmlRenderer: HtmlRenderer;
    let mailerService: MailerService;
    let userService: UserService;
    let clientError: jasmine.Spy;
    let serverError: jasmine.Spy;

    class MockResetPasswordRequest {
        body: ResetPasswordBodyViewModel;
        params: { token?: string } = {};
        user?: UserViewModel;
        login?: (user: User, callback: (err: any) => void) => void;
    }

    class MockResetPasswordResponse extends MockResponse {
        render?: (template: string, options: any, callback: (err: any, result: string) => void) => void;
    }

    beforeEach(async () => {
        container = await new SequelizeContainer().initialize();
        await new ErrorContainer().initialize(container);
        container.bind(HtmlRenderer).toSelf().inSingletonScope();
        container.bind<IMailerService>(MailerSymbols.MailerService).to(MockMailerService).inSingletonScope();
        container.bind(PasswordService).toSelf().inSingletonScope();
        container.bind(ResetPasswordController).toSelf();
        container.bind(UserService).toSelf().inSingletonScope();
        container.bind(UserRepository).toSelf().inSingletonScope();
        container.bind(UserViewModelMapper).toSelf();
        controller = container.get(ResetPasswordController);
        errorHandler = container.get(ErrorHandler);
        htmlRenderer = container.get(HtmlRenderer);
        mailerService = container.get(MailerSymbols.MailerService);
        userService = container.get(UserService);
        clientError = jasmine.createSpy();
        serverError = jasmine.createSpy();
    });

    it('should exist', () => {
        expect(controller).toBeTruthy();
    });

    describe('#validateResetToken', () => {
        it('should hit an error response if something happens', async () => {
            spyOn(errorHandler, 'sendUnknownServerErrorResponse');
            spyOn(userService, 'getUserByToken').and.throwError('oops');

            let request = new MockResetPasswordRequest();
            request.params.token = 'blahblahblah';

            await controller.validateResetToken(request.params.token, request as any, serverError);
            expect(serverError).toHaveBeenCalled();
        });

        it('should return a proper response if all is well', async () => {
            spyOn(userService, 'getUserByToken').and.returnValue(Promise.resolve(userService.createUserModel(
                TestHelpers.getTestUserAttributes()
            )));

            let request = new MockResetPasswordRequest();
            request.params.token = 'blahblahblah';

            await controller.validateResetToken(request.params.token, request as any, serverError);
            expect(serverError).not.toHaveBeenCalled();
        });
    });

    describe('#reset', () => {
        it('should detect if the user does not exist (token expired or invalid)', async () => {
            spyOn(errorHandler, 'sendClientErrorResponse');
            spyOn(userService, 'getUserByToken').and.returnValue(Promise.resolve(null));

            let request = new MockResetPasswordRequest();
            request.params.token = 'blahblahblah';

            await controller.reset(request.params.token, request as any, request.body, clientError, serverError);
            expect(clientError).toHaveBeenCalled();
        });

        it('should detect if the new passwords do not match', async () => {
            spyOn(errorHandler, 'sendClientErrorResponse');
            spyOn(userService, 'getUserByToken').and.returnValue(Promise.resolve(userService.createUserModel(
                TestHelpers.getTestUserAttributes()
            )));

            let request = new MockResetPasswordRequest();
            request.params.token = 'blahblahblah';
            request.body = {
                newPassword: 'newpassword',
                verifyPassword: 'badpassword'
            };

            await controller.reset(request.params.token, request as any, request.body, clientError, serverError);
            expect(clientError).toHaveBeenCalled();
        });

        it('should clear the token/expires', async () => {
            spyOn(errorHandler, 'sendClientErrorResponse');

            let mockModel = userService.createUserModel(TestHelpers.getTestUserAttributes());
            mockModel.resetPasswordToken = 'fdsfds';
            mockModel.resetPasswordExpires = new Date();

            spyOn(userService, 'getUserByToken').and.returnValue(Promise.resolve(mockModel));
            spyOn(userService, 'saveUser');

            let request = new MockResetPasswordRequest();
            request.login = (user, cb) => cb(null);
            request.params.token = 'blahblahblah';
            request.body = {
                newPassword: 'newpassword',
                verifyPassword: 'newpassword'
            };

            await controller.reset(request.params.token, request as any, request.body, clientError, serverError);

            expect(mockModel.resetPasswordExpires).toBeNull();
            expect(mockModel.resetPasswordToken).toBeNull();
            expect(userService.saveUser).toHaveBeenCalled();
        });

        it('should log the user in', async () => {
            let mockModel = userService.createUserModel(TestHelpers.getTestUserAttributes());

            spyOn(userService, 'getUserByToken').and.returnValue(Promise.resolve(mockModel));
            spyOn(userService, 'saveUser').and.returnValue(Promise.resolve(mockModel));
            spyOn(mailerService, 'send').and.returnValue(Promise.resolve() as any);

            let request = new MockResetPasswordRequest();
            request.params.token = 'blahblahblah';
            request.login = (user, callback) => callback(null);
            request.body = {
                newPassword: 'newpassword',
                verifyPassword: 'newpassword'
            };

            let result = await controller.reset(request.params.token, request as any, request.body, clientError, serverError);
            expect(result.id).toBe(mockModel.id);
        });

        it('should handle a login error', async () => {
            let mockModel = userService.createUserModel(TestHelpers.getTestUserAttributes());

            spyOn(userService, 'getUserByToken').and.returnValue(Promise.resolve(mockModel));
            spyOn(userService, 'saveUser').and.returnValue(Promise.resolve(mockModel));
            spyOn(mailerService, 'send').and.returnValue(Promise.resolve() as any);

            let request = new MockResetPasswordRequest();
            request.params.token = 'blahblahblah';
            request.login = (user, callback) => callback(true);
            request.body = {
                newPassword: 'newpassword',
                verifyPassword: 'newpassword'
            };

            let response = new MockResetPasswordResponse();
            response.render = (template, options, callback) => callback(null, 'woop');
            spyOn(response, 'jsonp');

            let fn = async () => await controller.reset(request.params.token, request as any, request.body, clientError, serverError);
            await expectAsync(fn()).toBeRejected();
        });

        it('should handle an email rendering error', async () => {
            let testUser = TestHelpers.getTestUserAttributes();
            let mockModel = userService.createUserModel(testUser);
            spyOn(userService, 'getUserByToken').and.returnValue(Promise.resolve(mockModel));

            let request = new MockResetPasswordRequest();
            request.params.token = 'blahblahblah';
            request.body = {
                newPassword: 'newpassword',
                verifyPassword: 'newpassword'
            };

            spyOn(htmlRenderer, 'render').and.throwError('woop');
            spyOn(mailerService, 'send');
            spyOn(userService, 'saveUser').and.returnValue(Promise.resolve(mockModel));

            await controller.reset(request.params.token, request as any, request.body, clientError, serverError);
            expect(mailerService.send).not.toHaveBeenCalled();
        });

        it('should send the email', async () => {
            let mockModel = userService.createUserModel(TestHelpers.getTestUserAttributes());
            spyOn(userService, 'getUserByToken').and.returnValue(Promise.resolve(mockModel));

            let request = new MockResetPasswordRequest();
            request.login = (user, cb) => cb(null);
            request.params.token = 'blahblahblah';
            request.body = {
                newPassword: 'newpassword',
                verifyPassword: 'newpassword'
            };

            let response = new MockResetPasswordResponse();
            response.render = (template, options, callback) => callback(null, 'woop');

            spyOn(mailerService, 'send');
            spyOn(userService, 'saveUser').and.returnValue(Promise.resolve(mockModel));

            await controller.reset(request.params.token, request as any, request.body, clientError, serverError);
            expect(mailerService.send).toHaveBeenCalled();
        });
    });
});
