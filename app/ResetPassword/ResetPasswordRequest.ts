import { KrossrRequest } from '../KrossrRequest/KrossrRequest';
import { ResetPasswordBodyViewModel } from './ResetPasswordBodyViewModel';

export interface ResetPasswordRequest extends KrossrRequest<any, ResetPasswordBodyViewModel> {
}
