import { DevelopmentSequelizeConfiguration } from './database/DevelopmentSequelizeConfiguration';
import { ProductionSequelizeConfiguration } from './database/ProductionSequelizeConfiguration';
import { TestSequelizeConfiguration } from './database/TestSequelizeConfiguration';

/** this is for sequelize-cli to be able to run migrations */

module.exports = {
    development: new DevelopmentSequelizeConfiguration(),
    test: new TestSequelizeConfiguration(),
    production: new ProductionSequelizeConfiguration()
};
