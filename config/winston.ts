import { KrossrLogger } from '../app/Logger/KrossrLogger';
import { KrossrLoggerProvider } from '../app/Logger/KrossrLoggerProvider';
import { LoggerSymbols } from '../app/Logger/LoggerSymbols';
import { provideSingleton } from '../app/Utils/provideSingleton';

let winston = require('winston');
let logger = new (winston.Logger)();

@provideSingleton(LoggerSymbols.KrossrLogger)
export class WinstonConfiguration implements KrossrLoggerProvider {
    private logger: KrossrLogger;

    getLogger() {
        return this.logger;
    }

    /** Should only be called by root file */
    initialize() {
        if (!!this.getLogger()) {
            throw new Error('this should only be called ONCE!');
        }

        logger.add(winston.transports.Console, {
            level: 'verbose',
            prettyPrint: true,
            colorize: true,
            silent: false,
            timestamp: false,
            verbose: false
        });

        logger.stream = {
            // jshint unused:false
            write(message: string){
                logger.info(message);
            }
        };

        this.logger = logger;
    }
}
