import { ProductionSequelizeConfiguration } from '../database/ProductionSequelizeConfiguration';
import { IEnvironmentConfiguration } from './IEnvironmentConfiguration';

export class ProductionEnvironmentConfiguration implements IEnvironmentConfiguration {
    app = {
        title: 'Krossr'
    };

    enableSequelizeLog = false;
    forceDbSync = process.env.FORCE_DB_SYNC === 'true';

    db = new ProductionSequelizeConfiguration();
    production = true;
}
