import { IApplicationConfiguration } from './IApplicationConfiguration';
import { IMailerOptions } from './IMailerOptions';
import { IDatabaseConfiguration } from './IDatabaseConfiguration';

export interface IEnvironmentConfiguration {
    app: IApplicationConfiguration;
    baseName?: string;
    db: IDatabaseConfiguration;
    forceDbSync?: boolean;
    mailer?: IMailerOptions;
    ipaddr?: string;
    port?: number;
    enableSequelizeLog: boolean;
    production?: boolean;
    templateEngine?: string;
}
