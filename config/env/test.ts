import { TestSequelizeConfiguration } from '../database/TestSequelizeConfiguration';
import { IEnvironmentConfiguration } from './IEnvironmentConfiguration';

export class TestEnvironmentConfiguration implements IEnvironmentConfiguration {
    app = {
        title: 'krossr - Test Environment'
    };

    enableSequelizeLog = false;

    db = new TestSequelizeConfiguration();
}
