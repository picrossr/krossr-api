import { DevelopmentSequelizeConfiguration } from '../database/DevelopmentSequelizeConfiguration';
import { IEnvironmentConfiguration } from './IEnvironmentConfiguration';

export class DevelopmentEnvironmentConfiguration implements IEnvironmentConfiguration {
    app = {
        title: 'krossr - Development Environment'
    };

    enableSequelizeLog = false;

    // forceDbSync = true;

    db = new DevelopmentSequelizeConfiguration();
}
