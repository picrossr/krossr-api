import { SequelizeConfiguration } from './SequelizeConfiguration';

export class DevelopmentSequelizeConfiguration implements SequelizeConfiguration {
    username = 'rosalyn';
    password = 'postgres123!';
    database = 'krossr';
    host = process.env.DB_HOST || '127.0.0.1';
    dialect: 'postgres' = 'postgres';
    port = 5432;
}
