import { SequelizeConfiguration } from './SequelizeConfiguration';

export class TestSequelizeConfiguration implements SequelizeConfiguration {
    dialect: 'postgres' = 'postgres';
    database = 'krossr-test';
    username = 'rosalyn';
    password = 'postgres123!';
    host = 'postgres';
    port = 5432;
}
