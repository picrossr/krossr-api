import { EnvironmentConfiguration } from './environmentConfig';
import express, { NextFunction, Request, Response } from 'express';
import * as passport from 'passport';
import { IKrossrDatabase } from '../app/Database/IKrossrDatabase';
import { multiInject, inject } from 'inversify';
import { RouteConfiguration } from '../app/Routes/RouteConfiguration';
import { RouteSymbols } from '../app/Routes/RouteSymbols';
import { IEnvironmentConfiguration } from './env/IEnvironmentConfiguration';
import { LoggerSymbols } from '../app/Logger/LoggerSymbols';
import { KrossrLoggerProvider } from '../app/Logger/KrossrLoggerProvider';
import { KrossrLogger } from '../app/Logger/KrossrLogger';
import { KrossrErrorResponse } from '../app/KrossrResponse/KrossrErrorResponse';
import { provideSingleton } from '../app/Utils/provideSingleton';
import { RegisterRoutes } from '../build/routes';
import { ValidateError } from 'tsoa';
import { ErrorResponse } from '../app/Error/ErrorResponse';

let cors = require('cors');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let session = require('express-session');
let compress = require('compression');
let methodOverride = require('method-override');
let helmet = require('helmet');
let SequelizeStore = require('connect-session-sequelize')(session.Store);
let flash = require('connect-flash');
let consolidate = require('consolidate');

@provideSingleton(ExpressConfiguration)
export class ExpressConfiguration {
    private config: IEnvironmentConfiguration;
    private logger: KrossrLogger;

    constructor(
        @inject(LoggerSymbols.KrossrLogger) private loggerProvider: KrossrLoggerProvider,
        @inject(EnvironmentConfiguration) private environmentConfiguration: EnvironmentConfiguration,
        @multiInject(RouteSymbols.RouteConfiguration) private routeConfigs: RouteConfiguration[],
    ) {
        this.config = this.environmentConfiguration.getConfiguration();
        this.logger = this.loggerProvider.getLogger();
    }

    configureCors(app: express.Express): void {
        const origin = `https://${this.config.baseName}.com`;
        const options = {
            origin,
            credentials: true
        };

        app.use(cors(options));
    }

    configureSessions(app: express.Express, store: any) {
        this.logger.info('Trying to set up sessions...');

        const sessionOptions = {
            store,
            secret: process.env.SESSION_SECRET,
            resave: true,
            cookie: {
                maxAge: 30 * 24 * 60 * 60 * 1000
            } as any, // 30 days
            saveUninitialized: true,
        };

        if (this.config.production) {
            app.set('trust proxy', 1); // trust first proxy
            sessionOptions.cookie.domain = `.${this.config.baseName}.com`; // allow any subdomain
            sessionOptions.cookie.secure = true; // enable https
            sessionOptions.cookie.sameSite = 'none';
        }

        // Postgres Sessions
        app.use(session(sessionOptions));

        this.logger.info('...done');
    }

    configure(db: IKrossrDatabase): express.Application {
        this.logger.info('Intializing Express!');

        // Initialize express app
        let app = express();

        // Passing the request url to environment locals
        app.use((req, res, next) => {
            res.locals.url = req.protocol + '://' + req.headers.host + req.url;
            next();
        });

        // Should be placed before express.static
        app.use(compress({
            filter(req: Request, res: Response) {
                return (/json|text|javascript|css/).test(res.getHeader('Content-Type') as string);
            },
            level: 9
        }));

        // Showing stack errors
        app.set('showStackError', true);

        // Set swig as the template engine
        app.engine('server.view.html', consolidate[this.config.templateEngine]);

        // Set views path and view engine
        app.set('view engine', 'server.view.html');
        app.set('views', './app/views');

        // Environment dependent middleware
        if (process.env.NODE_ENV === 'development') {
            // Enable logger (morgan)
            app.use(morgan('dev'));

            // Disable views cache
            app.set('view cache', false);
        } else if (process.env.NODE_ENV === 'production') {
            app.locals.cache = 'memory';
        }

        if (this.config.production) {
            this.configureCors(app);
        }

        // Request body parsing middleware should be above methodOverride
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json());
        app.use(methodOverride());

        // Enable jsonp
        app.enable('jsonp callback');

        let store = new SequelizeStore({ db: db.sequelize });

        if (this.config.forceDbSync) {
            store.sync();
        }

        this.configureSessions(app, store);

        // use passport session
        app.use(passport.initialize());
        app.use(passport.session());

        // connect flash for flash messages
        app.use(flash());

        // Use helmet to secure Express headers
        app.use(helmet.frameguard());
        app.use(helmet.xssFilter());
        app.use(helmet.noSniff());
        app.use(helmet.ieNoOpen());
        app.disable('x-powered-by');

        RegisterRoutes(app);
        this.routeConfigs.forEach(routeConfig => routeConfig.configureRoutes(app));

        app.use(function errorHandler(
            err: unknown,
            req: express.Request,
            res: express.Response,
            next: NextFunction
        ): express.Response | void {
            if (err instanceof ValidateError) {
                console.warn(`Caught Validation Error for ${req.path}:`, err.fields);

                let response: ErrorResponse = {
                    message: 'Validation Failed',
                    // details: err?.fields,
                };

                return res.status(422).json(response);
            }

            if (err instanceof Error) {
                console.warn('Caught Generic Error:', err.message);

                let response: ErrorResponse = {
                    message: 'Internal Server Error'
                };

                return res.status(500).json(response);
            }

            console.warn('No error handled...?');

            next();
        });

        // Assume 404 since no middleware responded
        app.use((req, res: KrossrErrorResponse) => {
            res.status(404).send({ message: 'Not Found' });
        });

        return app;
    }
}
