import * as _ from 'lodash';
import { Sequelize } from 'sequelize';
import { EnvironmentConfiguration } from './environmentConfig';
import * as sequelize from 'sequelize';
import { multiInject, inject } from 'inversify';
import { ModelSymbols } from '../app/models/ModelSymbols';
import { ModelConfiguration } from '../app/models/ModelConfiguration';
import { LoggerSymbols } from '../app/Logger/LoggerSymbols';
import { KrossrLoggerProvider } from '../app/Logger/KrossrLoggerProvider';
import { KrossrLogger } from '../app/Logger/KrossrLogger';
import Umzug from 'umzug';
import { provideSingleton } from '../app/Utils/provideSingleton';
const path = require('path');

@provideSingleton(SequelizeConfiguration)
export class SequelizeConfiguration {
    static initialized: boolean;
    static migrated: boolean;
    static seeded: boolean;

    private logger: KrossrLogger;

    constructor(
        @inject(LoggerSymbols.KrossrLogger) private loggerProvider: KrossrLoggerProvider,
        @inject(EnvironmentConfiguration) private environmentConfiguration: EnvironmentConfiguration,
        @multiInject(ModelSymbols.ModelConfiguration) private modelConfigs: ModelConfiguration<Sequelize>[]
    ) {
        this.logger = this.loggerProvider.getLogger();
    }

    async initialize() {
        if (SequelizeConfiguration.initialized) {
            return;
        }

        SequelizeConfiguration.initialized = true;

        let db: { sequelize?: sequelize.Sequelize } = {};

        this.logger.info('Initializing Sequelize...');

        let config = this.environmentConfiguration.getConfiguration();

        console.log(config.db.database);

        // create your instance of sequelize
        let database = new Sequelize(config.db.database, config.db.username, config.db.password, {
            host: config.db.host,
            port: config.db.port,
            dialect: config.db.dialect
        });


        this.modelConfigs.forEach((c) => {
            c.configure(database);
        });

        this.logger.info('Models initialized!');
        this.logger.info(`config.forceDbSync=${config.forceDbSync}`);
        this.logger.info(`FORCE_DB_SYNC=${process.env.FORCE_DB_SYNC}`);

        // Synchronizing any model changes with database.
        // set FORCE_DB_SYNC=true in the environment, or the program parameters to drop the database,
        //   and force model changes into it, if required;
        // Caution: Do not set FORCE_DB_SYNC to true for every run to avoid losing data with restarts
        await database
            .sync({
                force: config.forceDbSync,
                logging: config.enableSequelizeLog ? this.logger.verbose : false
            })
            .then(() => {
                this.logger.info('Database ' + (config.forceDbSync ? '*DROPPED* and ' : '') + 'synchronized');
            }).catch(err => {
                this.logger.error('An error occurred: ', err);
            });

        await this.migrate(database);
        this.logger.info('Migrated!');

        await this.seed(database);
        this.logger.info('Seeded!');

        // assign the sequelize variables to the db object and returning the db.
        db = _.extend({
            sequelize: database,
            Sequelize
        }, db);

        return db;
    }

    private async migrate(database: Sequelize) {
        if (SequelizeConfiguration.migrated) {
            return;
        }

        SequelizeConfiguration.migrated = true;

        const umzug = new Umzug({
            migrations: this.getMigrationOptions(database, '../migrations'),
            storage: 'sequelize',
            storageOptions: {
                sequelize: database
            }
        });

        await umzug.up();
    }

    private async seed(database: Sequelize) {
        if (SequelizeConfiguration.seeded || !process.env.NODE_ENV) {
            return;
        }

        SequelizeConfiguration.seeded = true;

        const umzug = new Umzug({
            migrations: this.getMigrationOptions(database, `../seeders/${process.env.NODE_ENV}`),
            storage: 'sequelize',
            storageOptions: {
                sequelize: database,
                modelName: 'SequelizeSeeders'
            }
        });

        await umzug.down({ to: 0 });
        await umzug.up();
    }

    private getMigrationOptions(database: Sequelize, rel: string) {
        return {
            path: path.join(__dirname, rel), params: [
                database.getQueryInterface()
            ]
        };
    }
}
