import { interfaces } from 'inversify';
import { ErrorHandler } from '../../app/Error/ErrorHandler';
import { ErrorMessageService } from '../../app/Error/ErrorMessageService';

export class ErrorContainer {
    async initialize(container: interfaces.Container): Promise<void> {
        container.bind(ErrorHandler).toSelf().inSingletonScope();
        container.bind(ErrorMessageService).toSelf();
    }
}
