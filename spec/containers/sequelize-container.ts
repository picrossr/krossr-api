import { Container } from 'inversify';
import { ModelConfiguration } from '../../app/models/ModelConfiguration';
import { Sequelize } from 'sequelize';
import { ModelSymbols } from '../../app/models/ModelSymbols';
import { RatingConfiguration } from '../../app/models/RatingModel';
import { UserConfiguration } from '../../app/models/UserModel';
import { LevelConfiguration } from '../../app/models/LevelModel';
import { SequelizeConfiguration } from '../../config/sequelizeConfig';
import { EnvironmentConfigurationDefaults } from '../../config/env/all';
import { EnvironmentConfiguration } from '../../config/environmentConfig';
import { KrossrLoggerProvider } from '../../app/Logger/KrossrLoggerProvider';
import { LoggerSymbols } from '../../app/Logger/LoggerSymbols';
import { MockLoggerProvider } from '../support/MockLogger';
import { AuthorizationService } from '../../app/Authorization/AuthorizationService';
import { BannedLayoutConfiguration } from '../../app/models/BannedLayoutModel';

export class SequelizeContainer {
    async initialize(): Promise<Container> {
        let sequelizeContainer = new Container();

        sequelizeContainer.bind<AuthorizationService>(AuthorizationService).toSelf().inSingletonScope();
        sequelizeContainer.get(AuthorizationService).configure();

        /** order */
        sequelizeContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(RatingConfiguration);
        sequelizeContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(UserConfiguration);
        sequelizeContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(LevelConfiguration);
        sequelizeContainer.bind<ModelConfiguration<Sequelize>>(ModelSymbols.ModelConfiguration).to(BannedLayoutConfiguration);
        /** is important */

        sequelizeContainer.bind<EnvironmentConfigurationDefaults>(EnvironmentConfigurationDefaults).toSelf();
        sequelizeContainer.bind<EnvironmentConfiguration>(EnvironmentConfiguration).toSelf().inSingletonScope();
        sequelizeContainer.bind<KrossrLoggerProvider>(LoggerSymbols.KrossrLogger).to(MockLoggerProvider);
        sequelizeContainer.bind(SequelizeConfiguration).toSelf().inSingletonScope();

        await sequelizeContainer.get(SequelizeConfiguration).initialize();

        return sequelizeContainer;
    }
}
