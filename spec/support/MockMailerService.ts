import { injectable } from 'inversify';
import { MailParams } from '../../app/Mailer/MailParams';
import { IMailerService } from '../../app/Mailer/IMailerService';

@injectable()
export class MockMailerService implements IMailerService {
    send(params: MailParams) {
        // do nothing
    }
}
