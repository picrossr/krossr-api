import { injectable } from 'inversify';
import { KrossrLoggerProvider } from '../../app/Logger/KrossrLoggerProvider';

@injectable()
export class MockLoggerProvider implements KrossrLoggerProvider {
    getLogger() {
        return {
            verbose: false,
            info: () => {},
            error: () => {}
        };
    }

    initialize() {
    }
}
