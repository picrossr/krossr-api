# Use the official node image to create a binary
FROM node:16

RUN node --version
RUN npm --version

# Environment variables
ARG DB_HOST
ARG SESSION_SECRET
ARG SENDGRID_API_KEY
ARG PG_PWD
ARG NODE_ENV
ARG PORT
ARG FORCE_DB_SYNC

RUN printenv
RUN pwd

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure copying both package.json AND package-lock.json (when available).
# Copying this first prevents re-running npm install on every code change.
COPY package*.json ./

# Install production dependencies.
# If you add a package-lock.json, speed your build by switching to 'npm ci'.
# RUN npm ci --only=production
RUN npm install

# Copy local code to the container image
COPY . ./

# Build the javascript
RUN npm run build

EXPOSE 80/tcp

CMD npm run start
