'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    queryInterface.describeTable('bannedLayouts')
      .then(async (def) => {
        if (def.message) {
          await Promise.resolve();
        } else {
          await queryInterface.addColumn(
            'bannedLayouts',
            'message',
            Sequelize.DataTypes.STRING
          )
        }
      });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn(
      'bannedLayouts',
      'message'
    )
  }
};
