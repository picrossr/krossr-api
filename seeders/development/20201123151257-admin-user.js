'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('users', [{
     id: 1,
     username: 'rosalyn',
     email: 'rrowe404@gmail.com',
     role: 'admin',
     salt: 'w8jLsOsB21OS5EN14ZhsHw==',
     hashedPassword: 'ADMYLWfpS+lqLJMGWi880C8ot6ViMenkDEhvre4BH6K9mIZZqftBuP2PJHq3V4to5C20eU2lbI0cOzpqXej+vQ==',
     provider: 'local',
     createdAt: new Date(),
     updatedAt: new Date()
   }]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', null, {});
  }
};
